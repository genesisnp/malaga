<?php
    include 'src/includes/header.php'
?>
    <main class="work-with-us">
        <section class="sct-banner sct-parallax">
            <div class="container-fluid">
                <div class="content-img npostulante">
                    <!--<img class="img-banner" src="assets/images/trabaja-con-nostros.jpg" alt="img/banner">-->
                </div>
                <div class="content-title-banner d-flex flex-column">
                    <h1 class="h1-banner text-right">TRABAJA CON<br>NOSOTROS</h1>
                    <a href="#" class="arrow-repeat no-border d-flex flex-column">
                        <img class="img-arrow-banner" src="assets/images/icons/slim-left.svg" alt="">
                        <!--<p class="p-scroll">scroll</p>-->
                    </a>
                </div>
            </div>
        </section>
        <section class="form-work-with-us sct-parallax">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-12 col-lg-10">
                        <div class="row">
                            <!--SECCION NAVBAR-FIXED PROYECTOS-->
                            <?php
                                include 'src/includes/navbar-trabaja-con-nosotros.php'
                            ?>
                            <div class="content-form col-12 col-lg-8 col-lg-9 wow slideInRight">
                                <div class="title-form d-flex align-items-center">
                                    <i class="icon-form-nav icon-proveedor pr-icon"></i>
                                    <h1 class="titles-big">Nuevo<br><span class="title-orange-clear">Proveedor</span>
                                    </h1>
                                </div>
                                <form action="#" class="form row" method="post" id="form-new-provider">
                                    <div class="form__wrapper col-12 col-lg-6">
                                        <input type="text" class="form-bg form__input" id="name-provider" name="name-provider">
                                        <label class="form__label">
                                            <span class="form__label-content">Nombre del Proveedor:</span>
                                        </label>
                                    </div>
                                    <div class="form__wrapper col-12 col-lg-6">
                                        <input type="text" class="form-bg form__input" id="ruc-provider" name="ruc-provider" maxlength="11" onkeypress="if (event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;">
                                        <label class="form__label">
                                            <span class="form__label-content">RUC:</span>
                                        </label>
                                    </div>
                                    <div class="form__wrapper col-12 col-lg-6">
                                        <input type="text" class="form-bg form__input" id="web-provider" name="web-provider">
                                        <label class="form__label">
                                            <span class="form__label-content">Página Web:</span>
                                        </label>
                                    </div>
                                    <div class="form__wrapper col-12 col-lg-6">
                                        <input type="text" class="form-bg form__input" id="name-contact-provider" name="name-contact-provider">
                                        <label class="form__label">
                                            <span class="form__label-content">Nombre de Contacto:</span>
                                        </label>
                                    </div>
                                    <div class="form__wrapper col-12 col-lg-6">
                                        <input type="text" class="form-bg form__input" id="phone-provider" name="phone-provider" maxlength="9" onkeypress="if (event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;">
                                        <label class="form__label">
                                            <span class="form__label-content">Teléfono:</span>
                                        </label>
                                    </div>
                                    <div class="form__wrapper col-12 col-lg-6">
                                        <input type="email" class="form-bg form__input" id="email-provider" name="email-provider">
                                        <label class="form__label">
                                            <span class="form__label-content">Email:</span>
                                        </label>
                                    </div>
                                    <div class="form__wrapper col-12">
                                        <input type="email" class="form-bg form__input" id="service-provider" name="service-provider">
                                        <label class="form__label">
                                            <span class="form__label-content">Producto y/o Servicio que Ofrece:</span>
                                        </label>
                                    </div>
                                    <div class="form-group col-12 d-flex justify-content-center justify-content-lg-end content-btn-send">
                                        <button type="submit" class="btn btn-send" id="btn-send-provider">Enviar</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <?php
            include 'src/includes/btn-flotant.php'
        ?>
    </main>
    <?php
        include 'src/includes/footer.php'
    ?>
    <script src="assets/js/libraries/jquery.validate.min.js"></script>
    <script src="assets/js/form-provider.js"></script>
</body>

</html>