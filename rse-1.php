<?php
    include 'src/includes/header.php'
?>
    <main id="main-rse" class="main-rse">
        <section class="sct-banner sct-parallax">
            <div class="container-fluid">
                <div class="content-img rse3">
                    <!--<img class="img-banner" src="assets/images/Novedades.jpg" alt="img/banner">-->
                </div>
                <div class="content-title-banner d-flex flex-column">
                    <h1 class="h1-banner">RSE-1</h1>
                    <a href="#sct-card-rse" data-ancla="sct-card-rse" class="arrow-repeat no-border d-flex flex-column">
                        <img class="img-arrow-banner" src="assets/images/icons/slim-left.svg" alt="">
                        <!--<p class="p-scroll">scroll</p>-->
                    </a>
                </div>
            </div>
        </section>

        <section class="sct-card-news" id="sct-card-rse">
            <!--SECCION NAVBAR-FIXED NOVEDADES-->
            <div class="container-fluid">
                <div class="row">
                    <?php
                        include 'src/includes/navbar-rse.php'
                    ?>
                    <div class="col-12 col-lg-9 col-xl-10 sct-parallax info-nav-news">
                        <div class="row mx-0">
                            <div class="col-12 d-flex align-items-center justify-content-between wrapper-title-pyts flex-column-reverse flex-lg-row">
                                <div class="content-title d-flex align-items-center wow zoomIn">
                                    <i class="icon-title-news icon-novedades color-icons"></i>
                                    <h1 class="titles-big title-orange-clear">RSE - 1</h1>
                                </div>
                            </div>
                            <div class="sct-card-news">
                                <div class="row">
                                    <div class="col-10 col-lg-12 col-xl-10 wrapper-card wow slideInUp">
                                        <div class="row align-items-center">
                                            <div class="col-12 col-lg-4 content-img px-0">
                                                <a href="detalle-rse.php">
                                                    <img class="img-card" src="assets/images/HOSPITAL-ESSALUD-PISCO.jpg" alt="img/card">
                                                </a>
                                            </div>
                                            <div class="col-12 col-lg-8 description-card">
                                                <div class="row">
                                                    <a href="detalle-rse.php" class="d-flex flex-column justify-content-center a-card">
                                                        <h1 class="title-card">Hospital de apoyo de Nazca</h1>
                                                        <div class="d-flex p-card">
                                                            <i class="icon-arrow"></i>
                                                            <p class="p-internas text-justify">Ampliación, mejoramiento y equipamiento la
                                                                infraestructura
                                                                del Hospital de Apoyo de Nasca.Lorem ipsum dolor sit amet, consectetuer
                                                                adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis
                                                                natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.
                                                            </p>
                                                        </div>
                                                    </a>
                                                    
                                                </div>
                                            </div>
                                            <div class="content-date d-flex justify-content-center align-items-center flex-column">
                                                <span class="date-span">16</span>
                                                <span class="date-span">MAY</span>
                                                <span class="date-span">2019</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-10 col-lg-12 col-xl-10 wrapper-card wow slideInUp">
                                        <div class="row align-items-center">
                                            <div class="col-12 col-lg-4 content-img px-0">
                                                <a href="detalle-rse.php">
                                                    <img class="img-card" src="assets/images/HOSPITAL-ESSALUD-PISCO.jpg" alt="img/card">
                                                </a>
                                            </div>
                                            <div class="col-12 col-lg-8 description-card">
                                                <div class="row">
                                                    <a href="detalle-rse.php" class="d-flex flex-column justify-content-center a-card">
                                                        <h1 class="title-card">Hospital de apoyo de Nazca</h1>
                                                        <div class="d-flex p-card">
                                                            <i class="icon-arrow"></i>
                                                            <p class="p-internas text-justify">Ampliación, mejoramiento y equipamiento la
                                                                infraestructura
                                                                del Hospital de Apoyo de Nasca.Lorem ipsum dolor sit amet, consectetuer
                                                                adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis
                                                                natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.
                                                            </p>
                                                        </div>
                                                    </a>
                                                    
                                                </div>
                                            </div>
                                            <div class="content-date d-flex justify-content-center align-items-center flex-column">
                                                <span class="date-span">16</span>
                                                <span class="date-span">MAY</span>
                                                <span class="date-span">2019</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-10 col-lg-12 col-xl-10 wrapper-card wow slideInUp">
                                        <div class="row align-items-center">
                                            <div class="col-12 col-lg-4 content-img px-0">
                                                <a href="detalle-rse.php">
                                                    <img class="img-card" src="assets/images/HOSPITAL-ESSALUD-PISCO.jpg" alt="img/card">
                                                </a>
                                            </div>
                                            <div class="col-12 col-lg-8 description-card">
                                                <div class="row">
                                                    <a href="detalle-rse.php" class="d-flex flex-column justify-content-center a-card">
                                                        <h1 class="title-card">Hospital de apoyo de Nazca</h1>
                                                        <div class="d-flex p-card">
                                                            <i class="icon-arrow"></i>
                                                            <p class="p-internas text-justify">Ampliación, mejoramiento y equipamiento la
                                                                infraestructura
                                                                del Hospital de Apoyo de Nasca.Lorem ipsum dolor sit amet, consectetuer
                                                                adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis
                                                                natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.
                                                            </p>
                                                        </div>
                                                    </a>
                                                    
                                                </div>
                                            </div>
                                            <div class="content-date d-flex justify-content-center align-items-center flex-column">
                                                <span class="date-span">16</span>
                                                <span class="date-span">MAY</span>
                                                <span class="date-span">2019</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-10 col-lg-12 col-xl-10 wrapper-card wow slideInUp">
                                        <div class="row align-items-center">
                                            <div class="col-12 col-lg-4 content-img px-0">
                                                <a href="detalle-rse.php">
                                                    <img class="img-card" src="assets/images/HOSPITAL-ESSALUD-PISCO.jpg" alt="img/card">
                                                </a>
                                            </div>
                                            <div class="col-12 col-lg-8 description-card">
                                                <div class="row">
                                                    <a href="detalle-rse.php" class="d-flex flex-column justify-content-center a-card">
                                                        <h1 class="title-card">Hospital de apoyo de Nazca</h1>
                                                        <div class="d-flex p-card">
                                                            <i class="icon-arrow"></i>
                                                            <p class="p-internas text-justify">Ampliación, mejoramiento y equipamiento la
                                                                infraestructura
                                                                del Hospital de Apoyo de Nasca.Lorem ipsum dolor sit amet, consectetuer
                                                                adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis
                                                                natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.
                                                            </p>
                                                        </div>
                                                    </a>
                                                    
                                                </div>
                                            </div>
                                            <div class="content-date d-flex justify-content-center align-items-center flex-column">
                                                <span class="date-span">16</span>
                                                <span class="date-span">MAY</span>
                                                <span class="date-span">2019</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>          
                        </div>
                    </div>
                </div>
            </div>
        </section>
        
        <?php
            include 'src/includes/btn-flotant.php'
        ?>
    </main>
    <?php
        include 'src/includes/footer.php'
    ?>
</body>

</html>