const btnModal = document.querySelectorAll('.btn-modal');
const modal = document.querySelector('.modal-content');
const modalCloseBtn = document.querySelector('#modal-close-btn');

btnModal.forEach((btnModals) => {
    btnModals.addEventListener('click', (e) => {
        modal.style.display = 'flex';
    })
})

modalCloseBtn.addEventListener('click', (e) => {
    modal.style.display = 'none';
});

modal.addEventListener('click', (e) => {
    if(e.target.classList.contains('modal-container')) {
        modal.style.display = 'none';
    }
});

$('.owl-img-popup').owlCarousel({
    loop:true,
    nav:true,
    loop: true,
    autoplay:true,
    animateOut: 'scale-carousel',
    stagePadding: 200,
    lazyLoad: true,
    items:1,
    navText: [
        '<img class="img-arrow-home" src="assets/images/icons/arrow-next-home.svg">',
        '<img class="img-arrow-home" src="assets/images/icons/arrow-prev-home.svg">'
    ],
    responsive:{
        0:{
            items:1,
            stagePadding: 0
        },
        600:{
            items:1,
            stagePadding: 0
        },
        1366:{
            items:1,
            stagePadding: 200
        }
    }
})