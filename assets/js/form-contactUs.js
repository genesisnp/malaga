$('.form__input').blur(function() {

    if ($(this).val()) {
        $(this).closest( '.form__wrapper' ).addClass('form--filled');
    } else {
        $(this).closest( '.form__wrapper' ).removeClass('form--filled');
    }	
});
$('#btn-contacUs').on('click', function(){
    $("#form-contactUs").validate({
        rules: {
            "name-contacUs": "required",
            "company-contacUs": "required",
            "message-contactUs":"required",
            "email-contactUs": {
                required: true,
                email: true
            },
            "phone-contactUs": {
                required: true,
                minlength: 7,
                maxlenght: 9
            }
        },
    });
})
