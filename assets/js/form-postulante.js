$('.form__input').blur(function() {

    if ($(this).val()) {
        $(this).closest( '.form__wrapper' ).addClass('form--filled');
    } else {
        $(this).closest( '.form__wrapper' ).removeClass('form--filled');
    }	
});
$('#btn-newApplicant').on('click', function(){
    $("#form-new-applicant").validate({
        rules: {
            "name-postulant": "required",
            "lastName-postulant": "required",
            "email-postulant": {
                required: true,
                email: true
            },
            "phone-postulant": {
                required: true,
                minlength: 7,
                maxlenght: 9
            },
            "dni-postulant": {
                required: true,
                maxlenght: 8
            } 
        },
    });
})
