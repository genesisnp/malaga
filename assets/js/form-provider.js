$('.form__input').blur(function() {

    if ($(this).val()) {
        $(this).closest( '.form__wrapper' ).addClass('form--filled');
    } else {
        $(this).closest( '.form__wrapper' ).removeClass('form--filled');
    }	
});
$('#btn-send-provider').on('click', function(){
    $("#form-new-provider").validate({
        rules: {
            "name-provider": "required",
            "name-contact-provider": "required",
            "service-provider": "required",
            "web-provider":"required",
            "ruc-provider": {
                required: true,
                maxlenght: 11
            },
            "email-provider": {
                required: true,
                email: true
            },
            "phone-provider": {
                required: true,
                minlength: 7,
                maxlenght: 9
            }
        },
    });
})
