//CAROUSEL DETALLE DE NOTICIA
$('.owl-img-detail').owlCarousel({
    loop:true,
    nav:true,
    loop: true,
    autoplay:true,
    animateOut: 'scale-carousel',
    items:1,
    navText: [
        '<img class="img-arrow-home" src="assets/images/icons/arrow-next-home.svg">',
        '<img class="img-arrow-home" src="assets/images/icons/arrow-prev-home.svg">'
    ]
})