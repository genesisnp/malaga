//modal 2 solo para que vea el cliente
const btnModal2 = document.querySelectorAll('.btn-modal-2');
const modal2 = document.querySelector('.modal-content-2');
const modalCloseBtn2 = document.querySelector('#modal-close-btn-2');
btnModal2.forEach((btnModalss) => {
    btnModalss.addEventListener('click', (e) => {
        modal2.style.display = 'flex';
    })
})

modalCloseBtn2.addEventListener('click', (e) => {
    modal2.style.display = 'none';
});

modal2.addEventListener('click', (e) => {
    if(e.target.classList.contains('modal-container')) {
        modal2.style.display = 'none';
    }
});