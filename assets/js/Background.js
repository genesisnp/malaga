function acglobalConstructor() {
    var self = this;
    /*cargar transicion*/this.cargarTransicion = function () {
        var transicion = function () {
            $("html").addClass("activa").trigger("activar");
        };
        $(window).on("load", transicion);
    };
    /*pata ancla*/this.pataAncla = function () {
        var ventana = $(window), ventanaAncho = ventana.outerWidth(true), cuerpo = $("body");
        var automatico = function () {
            $(".ancla_contenedor.pata_activo").each(function () {
                var esteContenedor = $(this), esteContenido = esteContenedor.children(".ancla_contenido"), esteAlto = esteContenido.outerHeight(true);
                esteContenedor.css({"height": esteAlto + "px"});
            });
            $(".ancla_contenedor:not(.pata_activo)").removeAttr("style");
        };
        automatico();
        ventana.on("load resize medir", automatico);
        setTimeout(automatico, 1000);

        cuerpo.on("mouseenter", ".ancla_pata[data-flota]", function (e) {
            if (ventanaAncho >= 1025) {
                var estePata = $(this), esteGrupo = estePata.data("grupo"), esteIndice = estePata.data("indice"), estosPatas = $(".ancla_pata.grupo" + esteGrupo + ".indice" + esteIndice), esteContenedor = $(".ancla_contenedor.grupo" + esteGrupo + ".indice" + esteIndice), esteContenido = esteContenedor.children(".ancla_contenido"), esteAlto = esteContenido.outerHeight(true), otrosPatas = $(".ancla_pata.grupo" + esteGrupo + ":not(.indice" + esteIndice + ")"), otrosContenedores = $(".ancla_contenedor.grupo" + esteGrupo + ":not(.indice" + esteIndice + ")");
                estosPatas.addClass("pata_activo");
                otrosPatas.removeClass("pata_activo");
                esteContenedor.addClass("pata_activo").css({"height": esteAlto + "px"});
                otrosContenedores.removeClass("pata_activo").removeAttr("style");
                ventana.trigger("medir");
            }
        });
    };

    this.init = function () {
        this.cargarTransicion();
    };
    return this.init();
}