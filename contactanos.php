<?php
    include 'src/includes/header.php'
?>
    <main id="contact-us">
        <section class="sct-banner sct-parallax">
            <div class="container-fluid">
                <div class="content-img">
                    <!--<img class="img-banner" src="assets/images/contactanos.jpg" alt="img/banner">-->
                </div>
                <div class="content-title-banner d-flex flex-column">
                    <h1 class="h1-banner text-right">CONTÁCTANOS</h1>
                    <a href="#" class="arrow-repeat no-border d-flex flex-column">
                        <img class="img-arrow-banner" src="assets/images/icons/slim-left.svg" alt="">
                        <!--<p class="p-scroll">scroll</p>-->
                    </a>
                </div>
            </div>
        </section>
        <section class="sct-content-form sct-parallax">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-12 col-xl-5 wow slideInLeft">
                        <div class="row">
                            <div class="title-form d-flex align-items-center col-12">
                                <i class="icon-nuestrosDatos"></i>
                                <h1 class="titles-big">Nuestros<br><span class="title-orange-clear">Datos</span>
                                </h1>
                            </div>
                            <div class="div-info col-12 col-lg-10 d-flex flex-column flex-xl-row">
                                <span class="title-info-form"><i class="icon-info-form icon-phone"></i>Teléfono: </span>
                                <p class="description-info-form p-internas mb-0">(511) 712 - 4200</p>
                            </div>
                            <div class="div-info col-12 col-lg-10 d-flex flex-column flex-xl-row">
                                <span class="title-info-form"><i class="icon-info-form icon-ubc"></i>Dirección: </span>
                                <p class="description-info-form p-internas mb-0">Av. Manuel Olguín 211 - Int. 1702
                                    Santiago de
                                    Surco - Lima, Perú</p>
                            </div>
                            <div class="div-info col-12 col-lg-10 d-flex flex-column flex-xl-row">
                                <span class="title-info-form"><i class="icon-info-form icon-sobre"></i>E-mail: </span>
                                <a href="mailto:constructoramalaga@c-malaga.com" class="description-info-form p-internas mb-0"> constructoramalaga@c-malaga.com</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-xl-6 wow slideInRight">
                        <form action="#" class="form row" method="post" id="form-contactUs">
                            <div class="form__wrapper col-12 col-lg-6">
                                <input type="text" class="form-bg form__input" id="name-contacUs" name="name-contacUs">
                                <label class="form__label">
                                    <span class="form__label-content">Nombres:</span>
                                </label>
                            </div>
                            <div class="form__wrapper col-12 col-lg-6">
                                <input type="text" class="form-bg form__input" id="company-contacUs" name="company-contacUs">
                                <label class="form__label">
                                    <span class="form__label-content">Empresa:</span>
                                </label>
                            </div>
                            <div class="form__wrapper col-12 col-lg-6">
                                <input type="text" class="form-bg form__input" id="phone-contactUs" name="phone-contactUs" maxlength="9" onkeypress="if (event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;">
                                <label class="form__label">
                                    <span class="form__label-content">Teléfono:</span>
                                </label>
                            </div>
                            <div class="form__wrapper col-12 col-lg-6">
                                <input type="email" class="form-bg form__input" id="email-contacUs" name="email-contactUs">
                                <label class="form__label">
                                    <span class="form__label-content">E-mail:</span>
                                </label>
                            </div>
                            <div class="form__wrapper col-12">
                               <textarea class="form_textarea form-bg" name="message-contactUs" id="message-contactUs"></textarea>
                                <label class="form__label">
                                    <span class="form__label-content">message:</span>
                                </label>
                            </div>
                            <div class="form-group col-12 d-flex justify-content-center justify-content-lg-end content-btn-send">
                                <button type="submit" class="btn btn-send" id="btn-contacUs">Enviar</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
        <section class="sct-map sct-parallax">
            <div class="container-fluid">
                <div class="row animatedParent animateOnce">
                    <div class="col-12 px-0 animated fadeInUpShort slower">
                        <iframe class="map"
                            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3901.390481220451!2d-76.97492538518708!3d-12.08539849144157!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9105c7ad00998c61%3A0x91c3d970502bcd9e!2sAvenida+Manuel+Olgu%C3%ADn+211%2C+Santiago+de+Surco+15023!5e0!3m2!1ses-419!2spe!4v1562620310759!5m2!1ses-419!2spe"
                            height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </section>
        <?php
            include 'src/includes/btn-flotant.php'
        ?>
    </main>
    <?php
        include 'src/includes/footer.php'
    ?>
    <script src="assets/js/libraries/jquery.validate.min.js"></script>
    <script src="assets/js/form-contactUs.js"></script>
</body>

</html>