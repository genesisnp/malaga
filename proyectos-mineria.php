<?php
    include 'src/includes/header.php'
?>
    <main class="proyects overflow-h">
        <section class="sct-banner sct-parallax">
            <div class="container-fluid">
                <div class="content-img">
                    <!--<img class="img-banner" src="assets/images/proyectos-.jpg" alt="img/banner">-->
                </div>
                <div class="content-title-banner d-flex flex-column">
                    <h1 class="h1-banner">PROYECTOS</h1>
                    <a href="#sct-card-proyects" data-ancla="sct-card-proyects" class="arrow-repeat no-border d-flex flex-column">
                        <img class="img-arrow-banner" src="assets/images/icons/slim-left.svg" alt="">
                        <!--<p class="p-scroll">scroll</p>-->
                    </a>
                </div>
            </div>
        </section>
        
        <!--SECCION DESCRIPTION PAGES NAV-PROYECTS-->
        <section class="sct-card-proyects sct-parallax" id="sct-card-proyects" name="sct-card-proyects">
            <div class="container-fluid">
                <div class="row">
                <!--SECCION NAVBAR-FIXED PROYECTOS-->
                    <?php
                        include 'src/includes/navbar-proyectos.php'
                    ?>
                    <div class="col-12 col-lg-8 col-xl-10 sct-parallax">
                        <div class="row">
                            <div class="wow zoomIn content-tform col-12 col-lg-11 d-flex align-items-center justify-content-between wrapper-title-pyts">
                                <div class="content-title d-flex align-items-center">
                                    <i class="icon-title-card icon-mineria"></i>
                                    <h1 class="titles-big title-orange-clear">Minería</h1>
                                </div>
                                <!-- <div class="wrapper-search d-flex justify-content-end">
                                    <form action="#" class="form-search">
                                        <div class="content-search d-flex align-items-center">
                                            <button class="icon-search"></button>
                                            <input class="input-search" type="search">
                                        </div>
                                    </form>
                                </div> -->
                            </div>
                            <!--CARDS-PROYECTS-->
                            <div class="col-12 col-xl-11 layout-cards-proyects d-flex flex-wrap justify-content-between">
                                <div class="row justify-content-center">
                                    <div class="col-11 col-md-6 col-xl-4 wrapper-card-proyect animated fadeInUpShort wow slideInUp" data-wow-duration="2s">
                                        <div class="post-module">
                                            <a href="detalle-proyecto.php"> 
                                                <div class="thumbnail">
                                                    <div class="date d-flex justify-content-center align-items-center">
                                                        <div class="day">27</div>
                                                        <div class="month">Mar</div>
                                                        <div class="year">2019</div>
                                                    </div>
                                                    <img class="img-thumbnail" src="assets/images/CARRETERA-JUNIN.jpg"/>
                                                </div>
                                                <div class="post-content d-flex justify-content-center align-items-center flex-column">
                                                    <h1 class="title title-blue">Carretera Ayacucho - Abancay</h1>
                                                    <h2 class="description d-flex">
                                                        <span class="icon-arrow"></span>
                                                        <p class="p-internas">Rehabilitación y Mejoramiento de la Carretera
                                                        Ayacucho-Abancay, Tramo : Dv. Kishuara - Puente Sahuinto.</p>
                                                    </h2>
                                                    <p class="vacio_post-content"></p>
                                                    <a href="detalle-proyecto.php" class="btn btn-vm title-blue">Ver más</a>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-11 col-md-6 col-xl-4 wrapper-card-proyect animated fadeInUpShort wow slideInUp" data-wow-delay="1s" data-wow-duration="2s">
                                        <div class="post-module">
                                            <a href="detalle-proyecto.php"> 
                                                <div class="thumbnail">
                                                    <div class="date d-flex justify-content-center align-items-center">
                                                        <div class="day">27</div>
                                                        <div class="month">Mar</div>
                                                        <div class="year">2019</div>
                                                    </div>
                                                    <img class="img-thumbnail" src="assets/images/CARRETERA-JUNIN.jpg"/>
                                                </div>
                                                <div class="post-content d-flex justify-content-center align-items-center flex-column">
                                                    <h1 class="title title-blue">Carretera Ayacucho - Abancay</h1>
                                                    <h2 class="description d-flex">
                                                        <span class="icon-arrow"></span>
                                                        <p class="p-internas">Rehabilitación y Mejoramiento de la Carretera
                                                        Ayacucho-Abancay, Tramo : Dv. Kishuara - Puente Sahuinto.</p>
                                                    </h2>
                                                    <p class="vacio_post-content"></p>
                                                    <a href="detalle-proyecto.php" class="btn btn-vm title-blue">Ver más</a>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-11 col-md-6 col-xl-4 wrapper-card-proyect animated fadeInUpShort wow slideInUp" data-wow-delay="2s" data-wow-duration="2s">
                                        <div class="post-module">
                                            <a href="detalle-proyecto.php"> 
                                                <div class="thumbnail">
                                                    <div class="date d-flex justify-content-center align-items-center">
                                                        <div class="day">27</div>
                                                        <div class="month">Mar</div>
                                                        <div class="year">2019</div>
                                                    </div>
                                                    <img class="img-thumbnail" src="assets/images/CARRETERA-JUNIN.jpg"/>
                                                </div>
                                                <div class="post-content d-flex justify-content-center align-items-center flex-column">
                                                    <h1 class="title title-blue">Carretera Ayacucho - Abancay</h1>
                                                    <h2 class="description d-flex">
                                                        <span class="icon-arrow"></span>
                                                        <p class="p-internas">Rehabilitación y Mejoramiento de la Carretera
                                                        Ayacucho-Abancay, Tramo : Dv. Kishuara - Puente Sahuinto.</p>
                                                    </h2>
                                                    <p class="vacio_post-content"></p>
                                                    <a href="detalle-proyecto.php" class="btn btn-vm title-blue">Ver más</a>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-11 col-md-6 col-xl-4 wrapper-card-proyect animated fadeInUpShort wow slideInUp" data-wow-duration="2s">
                                        <div class="post-module">
                                            <a href="detalle-proyecto.php"> 
                                                <div class="thumbnail">
                                                    <div class="date d-flex justify-content-center align-items-center">
                                                        <div class="day">27</div>
                                                        <div class="month">Mar</div>
                                                        <div class="year">2019</div>
                                                    </div>
                                                    <img class="img-thumbnail" src="assets/images/CARRETERA-JUNIN.jpg"/>
                                                </div>
                                                <div class="post-content d-flex justify-content-center align-items-center flex-column">
                                                    <h1 class="title title-blue">Carretera Ayacucho - Abancay</h1>
                                                    <h2 class="description d-flex">
                                                        <span class="icon-arrow"></span>
                                                        <p class="p-internas">Rehabilitación y Mejoramiento de la Carretera
                                                        Ayacucho-Abancay, Tramo : Dv. Kishuara - Puente Sahuinto.</p>
                                                    </h2>
                                                    <p class="vacio_post-content"></p>
                                                    <a href="detalle-proyecto.php" class="btn btn-vm title-blue">Ver más</a>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-11 col-md-6 col-xl-4 wrapper-card-proyect animated fadeInUpShort wow slideInUp" data-wow-delay="1s" data-wow-duration="2s">
                                        <div class="post-module">
                                            <a href="detalle-proyecto.php"> 
                                                <div class="thumbnail">
                                                    <div class="date d-flex justify-content-center align-items-center">
                                                        <div class="day">27</div>
                                                        <div class="month">Mar</div>
                                                        <div class="year">2019</div>
                                                    </div>
                                                    <img class="img-thumbnail" src="assets/images/CARRETERA-JUNIN.jpg"/>
                                                </div>
                                                <div class="post-content d-flex justify-content-center align-items-center flex-column">
                                                    <h1 class="title title-blue">Carretera Ayacucho - Abancay</h1>
                                                    <h2 class="description d-flex">
                                                        <span class="icon-arrow"></span>
                                                        <p class="p-internas">Rehabilitación y Mejoramiento de la Carretera
                                                        Ayacucho-Abancay, Tramo : Dv. Kishuara - Puente Sahuinto.</p>
                                                    </h2>
                                                    <p class="vacio_post-content"></p>
                                                    <a href="detalle-proyecto.php" class="btn btn-vm title-blue">Ver más</a>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-11 col-md-6 col-xl-4 wrapper-card-proyect animated fadeInUpShort wow slideInUp" data-wow-delay="2s" data-wow-duration="2s">
                                        <div class="post-module">
                                            <a href="detalle-proyecto.php"> 
                                                <div class="thumbnail">
                                                    <div class="date d-flex justify-content-center align-items-center">
                                                        <div class="day">27</div>
                                                        <div class="month">Mar</div>
                                                        <div class="year">2019</div>
                                                    </div>
                                                    <img class="img-thumbnail" src="assets/images/CARRETERA-JUNIN.jpg"/>
                                                </div>
                                                <div class="post-content d-flex justify-content-center align-items-center flex-column">
                                                    <h1 class="title title-blue">Carretera Ayacucho - Abancay</h1>
                                                    <h2 class="description d-flex">
                                                        <span class="icon-arrow"></span>
                                                        <p class="p-internas">Rehabilitación y Mejoramiento de la Carretera
                                                        Ayacucho-Abancay, Tramo : Dv. Kishuara - Puente Sahuinto.</p>
                                                    </h2>
                                                    <p class="vacio_post-content"></p>
                                                    <a href="detalle-proyecto.php" class="btn btn-vm title-blue">Ver más</a>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <?php
            include 'src/includes/btn-flotant.php'
        ?>
    </main>
    <?php
        include 'src/includes/footer.php'
    ?>
    <script src="assets/js/navbar-proyect.js"></script>
    
</body>

</html>