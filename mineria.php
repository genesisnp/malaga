<?php
    include 'src/includes/header.php'
?>
    <main class="main-bussines-line">
        <section class="sct-banner sct-parallax">
            <div class="container-fluid">
                <div class="content-img">
                    <!--<img class="img-banner" src="assets/images/mineria.jpg" alt="img/banner">-->
                </div>
                <div class="content-title-banner d-flex flex-column">
                    <h1 class="h1-banner">Minería</h1>
                    <a href="#sct-carousel" data-ancla="sct-carousel" class="arrow-repeat no-border d-flex flex-column">
                        <img class="img-arrow-banner" src="assets/images/icons/slim-left.svg" alt="">
                        <!--<p class="p-scroll">scroll</p>-->
                    </a>
                </div>
            </div>
        </section>

        <section class="sct-carousel sct-parallax" id="sct-carousel" name="sct-carousel">
            <div class="container-fluid">
                <div class="row align-items-center justify-content-center">
                    <!--CAROUSEL DONDE SE MUESTRA TEXTO DE PROYECTOS-->
                    <div class="col-12 col-lg-7 col-xl-5 info-carousel">
                        <div class="row flex-column justify-content-center align-items-center">
                            <div class="col-12 col-lg-9 col-xl-6 wow slideInLeft">
                                <div class="row">
                                    <div class="col-12 d-flex align-items-center content-title-carousel">
                                        <i class="icon-carousel icon-mineria"></i>
                                        <h1 class="titles-big title-orange-clear">MINERÍA</h1>
                                    </div>
                                    <div id="#" class="owl-content-info col-12">
                                        <div class="item d-flex">
                                            <div class="d-flex flex-column description-info-carousel">
                                                <p class="p-internas">Construcción de PAD de Lixiviación para Minera
                                                    ampa de Cobre.</p>
                                                <p class="p-internas">Lorem ipsum dolor sit amet, consectetuer
                                                    adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum
                                                    sociis natoque penatibus et magnis dis parturient montes, nascetur
                                                    ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu,
                                                    pretium quis, sem.</p>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="col-9 col-md-6 col-lg-12 title-orange-clear content-vp text-center">
                                        <i class="icon-diamonds"></i>
                                        <a href="proyectos-mineria.php" class="title-orange-clear vp">Ver Proyectos</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--CAROUSEL DONDE SE MUESTRA IMG-->
                    <div class="wow slideInRight col-12 col-lg-5 col-xl-7 content-img-carousel px-0">
                        <div id="carousel-img-repeat"
                            class="carousel-img-repeat owl-img-carousel owl-carousel owl-theme">
                            <div class="item">
                                <img src="assets/images/CARRETERA-JUNIN.jpg" alt="">
                            </div>
                            <div class="item">
                                <img src="assets/images/Carretera-Tocache.jpg" alt="">
                            </div>
                            <div class="item">
                                <img src="assets/images/HOSPITAL-ESSALUD-PISCO.jpg" alt="">
                            </div>
                            <div class="item">
                                <img src="assets/images/slider-home1.jpg" alt="">
                            </div>
                            <div class="item">
                                <img src="assets/images/Novedades.jpg" alt="">
                            </div>
                        </div>
                        <div id="counter" class=" counter d-flex align-items-center justify-content-center">
                            <h1 class="item-actual my-0"></h1>
                            <span class="slash"></span>
                            <h1 class="total-items my-0"></h1>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <?php
            include 'src/includes/btn-flotant.php'
        ?>
    </main>
    <?php
        include 'src/includes/footer.php'
    ?>
</body>

</html>