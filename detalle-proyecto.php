<?php
    include 'src/includes/header.php'
?>
    <main class="detail-proyects">
        <section class="sct-carousel">
            <div class="container-fluid">
                <div class="row justify-content-center">
                    <!--CAROUSEL DONDE SE MUESTRA TEXTO DE PROYECTOS-->
                    <div class="col-12 col-xl-5 info-carousel">
                        <div class="row flex-column align-items-center">
                            <div class=" wow slideInLeft col-12 col-xl-6">
                                <div class="row">
                                    <div class="col-12 d-flex title-link-detail flex-column">
                                        <a href="proyectos-mineria.php" class="icon-arrow-link"><span class="a-volver">Volver</span></a>
                                        <div class="d-flex align-items-center">
                                            <i class="icon-detail icon-mineria"></i>
                                            <span class="title-orange-clear">Minería</span>
                                        </div>
                                    </div>

                                    <div id="#" class="owl-content-info col-12">
                                        <div class="item d-flex">
                                            <div class="d-flex flex-column description-info-carousel">
                                                <h1 class="title">Carretera<br><span>Ayacucho-Abancay</span></h1>
                                                <div class="d-flex">
                                                    <i class="icon-arrow"></i>
                                                    <div>
                                                        <p class="p-internas">Construcción de PAD de Lixiviación para
                                                            Minera pampa de Cobre. Lorem ipsum dolor sit amet, consectetuer
                                                            adipiscing elit. Aenean commodo ligula eget dolor. Aenean
                                                            massa.</p>
                                                    </div>
                                                </div>
                                                <div class="info-general-proyc">
                                                    <div>
                                                        <h1 class="t-proy">Cliente: </h1>
                                                        <p class="p-internas">Lorem ipsum dolor sit amet</p>
                                                    </div>
                                                    <div>
                                                        <h1 class="t-proy">Periodo: </h1>
                                                        <p class="p-internas">Lorem ipsum dolor sit amet</p>
                                                    </div>
                                                    <div>
                                                        <h1 class="t-proy">Ubicación: </h1>
                                                        <p class="p-internas">Lorem - ipsum</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--CAROUSEL DONDE SE MUESTRA IMG-->
                    <div class="wow slideInRight col-12 col-xl-7 content-img-carousel px-0">
                        <div id="carousel-img-repeat"
                            class="carousel-img-repeat owl-img-carousel owl-carousel owl-theme">
                            <div class="item">
                                <img src="assets/images/CARRETERA-JUNIN.jpg" alt="">
                                <div class="state-of-work">
                                    <h1 class="title-state">OBRA CULMINADA</h1>
                                </div>
                            </div>
                            <div class="item">
                                <img src="assets/images/Carretera-Tocache.jpg" alt="">
                                <div class="state-of-work">
                                    <h1 class="title-state">OBRA CULMINADA</h1>
                                </div>
                            </div>
                            <div class="item">
                                <img src="assets/images/HOSPITAL-ESSALUD-PISCO.jpg" alt="">
                                <div class="state-of-work">
                                    <h1 class="title-state">OBRA CULMINADA</h1>
                                </div>
                            </div>
                            <div class="item">
                                <img src="assets/images/slider-home1.jpg" alt="">
                                <div class="state-of-work">
                                    <h1 class="title-state">OBRA CULMINADA</h1>
                                </div>
                            </div>
                            <div class="item">
                                <img src="assets/images/Novedades.jpg" alt="">
                                <div class="state-of-work">
                                    <h1 class="title-state">OBRA CULMINADA</h1>
                                </div>
                            </div>
                        </div>
                        <div id="counter" class=" counter d-flex align-items-center justify-content-center">
                            <h1 class="item-actual my-0"></h1>
                            <span class="slash"></span>
                            <h1 class="total-items my-0"></h1>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <?php
            include 'src/includes/btn-flotant.php'
        ?>
        <?php
            include 'src/includes/footer.php'
        ?>
    </main>
    
</body>

</html>