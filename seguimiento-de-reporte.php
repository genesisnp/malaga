<?php
    include 'src/includes/header.php'
?>
    <main id="report-segment">
        <!--BANNER-->
        <section class="sct-banner">
            <div class="container-fluid">
                <div class="content-img">
                    <img class="img-banner" src="assets/images/canal-de-inquietudes.jpg" alt="img/banner">
                </div>
                <div class="content-title-banner d-flex flex-column">
                    <h1 class="h1-banner text-right">CANAL DE</h1>
                    <h1 class="h1-banner two text-right">INQUIETUDES</h1>
                    <a href="#" class="arrow-repeat no-border d-flex flex-column">
                        <img class="img-arrow-banner" src="assets/images/icons/slim-left.svg" alt="">
                        <!--<p class="p-scroll">scroll</p>-->
                    </a>
                </div>
            </div>
        </section>
        <section class="sct-form-segment">
            <div class="container">
                <div class="row flex-column justify-content-center align-items-center">
                    <div class="col-12 col-lg-6 text-center">
                        <i class="icon-seguimiento color-icons wow zoomIn"></i>
                        <h1 class="titles-big title-orange-clear wow zoomIn">SEGUIMIENTO DE REPORTE</h1>
                        <p class="p-internas animated fadeInUpShort slower 250">En caso que desee consultar el estado de un reporte por favor ingrese
                            código que le fue asignado al registrarlo:</p>
                        <form class="content-form row justify-content-center form-seg-report">
                            <div class="form__wrapper col-12 col-lg-10">
                                <input type="text" class="form-bg form__input" id="number-segment" name="number-segment">
                                <label class="form__label">
                                    <span class="form__label-content">Ingresa tu número de Reporte</span>
                                </label>
                            </div>
                            <div class="form-group col-12 d-flex justify-content-center content-btn-send">
                                <a href="#" type="submit" class="btn btn-send d-flex justify-content-center align-items-center">Consultar</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
        <?php
            include 'src/includes/btn-flotant.php'
        ?>
    </main>
    <?php
        include 'src/includes/footer.php'
    ?>
    <script src="assets/js/libraries/jquery.validate.min.js"></script>
    <script src="assets/js/form-report.js"></script>
</body>

</html>