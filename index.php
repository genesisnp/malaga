<?php
    include 'src/includes/header.php'
?>
<?php if ($TipoForm == "rse12345") { ?>
        <link rel="stylesheet" href="<?php echo Dominio; ?>assets/css/app_copia.css">
    <?php } else { ?> 
        <link rel="stylesheet" href="<?php echo Dominio; ?>assets/css/app.css">
    <?php }?>
    <div id="fullpage" class="main-repeat main-home">
        <!--SECCIÓN ONE-->
        <section class="section sct-one-home">
            <div class="container-fluid">
                <div class="row">
                    <div id="owl-carousel-principal" class="owl-carousel">
                        <div class="item">
                            <div class="text">
                                <h1 class="titles-big">Calidad</h1>
                                <p class="p-internas text-center">Contamos con profesionales altamente calificados ejecutando
                                    proyectos siguiendo los más estrictos estándares internacionales de calidad
                                    (<strong>ISO 9001, 37001 y OHSAS 18001</strong>)</p>
                            </div>
                            <img src="assets/images/slider-home1.jpg" />
                            <div class="bg-home-s1"></div>
                        </div>
                        <div class="item">
                            <div class="text">
                                <h1 class="titles-big">Calidad 2</h1>
                                <p class="p-internas text-center">Contamos con profesionales altamente calificados ejecutando
                                    proyectos siguiendo los más estrictos estándares internacionales de calidad
                                    (<strong>ISO 9001, 37001 y OHSAS 18001</strong>)</p>
                            </div>
                            <img src="assets/images/home-two.jpg" />
                            <div class="bg-home-s1"></div>
                        </div>
                        <div class="item">
                            <div class="text">
                                <h1 class="titles-big">Calidad 3</h1>
                                <p class="p-internas text-center">Contamos con profesionales altamente calificados ejecutando
                                    proyectos siguiendo los más estrictos estándares internacionales de calidad
                                    (<strong>ISO 9001, 37001 y OHSAS 18001</strong>)</p>
                            </div>
                            <img src="assets/images/slider-home3.jpg" />
                            <div class="bg-home-s1"></div>
                        </div>

                    </div>
                    <div class="content-title-banner d-flex flex-column">
                        <a href="#sct-category-products" data-ancla="sct-category-products"class="arrow-repeat no-border d-flex flex-column">
                            <img class="img-arrow-banner" src="assets/images/icons/slim-left.svg" alt="">
                            <!--<p class="p-scroll">scroll</p>-->
                        </a>
                    </div>
                </div>
            </div>
            <div class="bg-sombra"></div>
        </section>
        <!--SECCIÓN TWO HOME (DIAMOND BUSINESS LINE)-->
        <?php
            include 'src/includes/diamonds-lineas-negocio.php'
        ?>
        <!--SECCIÓN THREE HOME (CAROUSEL-PROYECTS)-->
        <section class="section sct-carousel">
            <div class="container-fluid home-crsl">
                <div class="row align-items-center justify-content-center">
                    <!--CAROUSEL DONDE SE MUESTRA TEXTO DE PROYECTOS-->
                    <div class="col-12 col-lg-5 info-carousel">
                        <div class="row flex-column justify-content-center align-items-center">
                            <div class="col-12 col-lg-6 wow slideInLeft">
                                <div class="row">
                                    <div class="col-12 content-title-carousel ">
                                        <h1 class="titles-big">NUESTROS<br><span
                                                class="title-orange-clear">PROYECTOS</span>
                                        </h1>
                                    </div>
                                    <div id="carousel-home-info" class="owl-content-info owl-carousel owl-theme col-12">
                                        <div class="item d-flex">
                                            <i class="icon-arrow"></i>
                                            <div class="d-flex flex-column description-info-carousel">
                                                <h1 class="title">Carretera<br><span>Ayacucho-Abancay</span></h1>
                                                <p class="p-internas">Rehabilitación y Mejoramiento de la Carretera
                                                    Ayacucho -
                                                    Abancay, Tramo: Dv. Kisshuara - Puente Sahuinto.</p>
                                            </div>
                                        </div>
                                        <div class="item d-flex">
                                            <i class="icon-arrow"></i>
                                            <div class="d-flex flex-column description-info-carousel">
                                                <h1 class="title">Carretera Ayacucho-Abancay 2</h1>
                                                <p class="p-internas">Lorem ipsum dolor sit amet consectetur adipisicing elit consectetur adipisicing consectetur adipisicing.</p>
                                            </div>
                                        </div>
                                        <div class="item d-flex">
                                            <i class="icon-arrow"></i>
                                            <div class="d-flex flex-column description-info-carousel">
                                                <h1 class="title">Hospital ESSALUD - Junín</h1>
                                                <p class="p-internas">Rehabilitación y Mejoramiento de la Carretera
                                                    Ayacucho -
                                                    Abancay, Tramo: Dv. Kisshuara - Puente Sahuinto.</p>
                                            </div>
                                        </div>
                                        <div class="item d-flex">
                                            <i class="icon-arrow"></i>
                                            <div class="d-flex flex-column description-info-carousel">
                                                <h1 class="title">Carretera<br><span>Ayacucho-Abancay 4</span></h1>
                                                <p class="p-internas">Lorem ipsum dolor sit amet consectetur adipisicing elit consectetur adipisicing consectetur adipisicing.</p>
                                            </div>
                                        </div>
                                        <div class="item d-flex">
                                            <i class="icon-arrow"></i>
                                            <div class="d-flex flex-column description-info-carousel">
                                                <h1 class="title">Carretera<br><span>Ayacucho-Abancay 5</span></h1>
                                                <p class="p-internas">Rehabilitación y Mejoramiento de la Carretera
                                                    Ayacucho -
                                                    Abancay, Tramo: Dv. Kisshuara - Puente Sahuinto.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--CAROUSEL DONDE SE MUESTRA IMG-->
                    <div class="col-12 col-lg-7 content-img-carousel px-0 wow slideInRight">
                        <div id="carousel-img-home" class="owl-img-carousel owl-carousel owl-theme">
                            <div class="item">
                                <img src="assets/images/CARRETERA-JUNIN.jpg" alt="">
                            </div>
                            <div class="item">
                                <img src="assets/images/Carretera-Tocache.jpg" alt="">
                            </div>
                            <div class="item">
                                <img src="assets/images/HOSPITAL-ESSALUD-PISCO.jpg" alt="">
                            </div>
                            <div class="item">
                                <img src="assets/images/slider-home1.jpg" alt="">
                            </div>
                            <div class="item">
                                <img src="assets/images/Novedades.jpg" alt="">
                            </div>
                        </div>
                        <div id="counter" class="counter d-flex align-items-center justify-content-center">
                            <h1 class="item-actual my-0"></h1>
                            <span class="slash"></span>
                            <h1 class="total-items my-0"></h1>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--SECCION NOVEDADES-->
        <section class="sct-card-news container">
            <div class="row ">
                <div class="content-title-home d-flex align-items-center justify-content-center col-12">
                    <h1 class="titles-big title-orange-clear wow zoomIn">NOVEDADES</h1>
                </div>
                <div class=" wow fadeInUp col-11 col-md-6 col-lg-4 col-xl-3 wrapper-card-proyect">
                    <div class="post-module">
                        <a href="detalle-novedades.php"> 
                            <div class="thumbnail">
                                <div class="date d-flex justify-content-center align-items-center">
                                    <div class="day">27</div>
                                    <div class="month">Mar</div>
                                    <div class="year">2019</div>
                                </div>
                                <img class="img-thumbnail" src="assets/images/CARRETERA-JUNIN.jpg"/>
                            </div>
                            <div class="post-content d-flex justify-content-center align-items-center flex-column">
                                <h1 class="title title-blue">Carretera Ayacucho - Abancay</h1>
                                <h2 class="description d-flex">
                                    <span class="icon-arrow"></span>
                                    <p class="p-internas">Rehabilitación y Mejoramiento de la Carretera
                                    Ayacucho-Abancay, Tramo : Dv. Kishuara - Puente Sahuinto.</p>
                                </h2>
                                <p class="vacio_post-content"></p>
                                <a href="detalle-novedades.php" class="btn btn-vm title-blue">Ver más</a>
                            </div>
                        </a>
                    </div>
                </div>
                <div class=" wow fadeInUp col-11 col-md-6 col-lg-4 col-xl-3 wrapper-card-proyect">
                    <div class="post-module">
                        <a href="detalle-novedades.php"> 
                            <div class="thumbnail">
                                <div class="date d-flex justify-content-center align-items-center">
                                    <div class="day">27</div>
                                    <div class="month">Mar</div>
                                    <div class="year">2019</div>
                                </div>
                                <img class="img-thumbnail" src="assets/images/CARRETERA-JUNIN.jpg"/>
                            </div>
                            <div class="post-content d-flex justify-content-center align-items-center flex-column">
                                <h1 class="title title-blue">Carretera Ayacucho - Abancay</h1>
                                <h2 class="description d-flex">
                                    <span class="icon-arrow"></span>
                                    <p class="p-internas">Rehabilitación y Mejoramiento de la Carretera
                                    Ayacucho-Abancay, Tramo : Dv. Kishuara - Puente Sahuinto.</p>
                                </h2>
                                <p class="vacio_post-content"></p>
                                <a href="detalle-novedades.php" class="btn btn-vm title-blue">Ver más</a>
                            </div>
                        </a>
                    </div>
                </div>
                <div class=" wow fadeInUp col-11 col-md-6 col-lg-4 col-xl-3 wrapper-card-proyect">
                    <div class="post-module">
                        <a href="detalle-novedades.php"> 
                            <div class="thumbnail">
                                <div class="date d-flex justify-content-center align-items-center">
                                    <div class="day">27</div>
                                    <div class="month">Mar</div>
                                    <div class="year">2019</div>
                                </div>
                                <img class="img-thumbnail" src="assets/images/CARRETERA-JUNIN.jpg"/>
                            </div>
                            <div class="post-content d-flex justify-content-center align-items-center flex-column">
                                <h1 class="title title-blue">Carretera Ayacucho - Abancay</h1>
                                <h2 class="description d-flex">
                                    <span class="icon-arrow"></span>
                                    <p class="p-internas">Rehabilitación y Mejoramiento de la Carretera
                                    Ayacucho-Abancay, Tramo : Dv. Kishuara - Puente Sahuinto.</p>
                                </h2>
                                <p class="vacio_post-content"></p>
                                <a href="detalle-novedades.php" class="btn btn-vm title-blue">Ver más</a>
                            </div>
                        </a>
                    </div>
                </div>
                <div class=" wow fadeInUp col-11 col-md-6 col-lg-4 col-xl-3 wrapper-card-proyect">
                    <div class="post-module">
                        <a href="detalle-novedades.php"> 
                            <div class="thumbnail">
                                <div class="date d-flex justify-content-center align-items-center">
                                    <div class="day">27</div>
                                    <div class="month">Mar</div>
                                    <div class="year">2019</div>
                                </div>
                                <img class="img-thumbnail" src="assets/images/CARRETERA-JUNIN.jpg"/>
                            </div>
                            <div class="post-content d-flex justify-content-center align-items-center flex-column">
                                <h1 class="title title-blue">Carretera Ayacucho - Abancay</h1>
                                <h2 class="description d-flex">
                                    <span class="icon-arrow"></span>
                                    <p class="p-internas">Rehabilitación y Mejoramiento de la Carretera
                                    Ayacucho-Abancay, Tramo : Dv. Kishuara - Puente Sahuinto.</p>
                                </h2>
                                <p class="vacio_post-content"></p>
                                <a href="detalle-novedades.php" class="btn btn-vm title-blue">Ver más</a>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </section> 
        <!--CERTIFICACIONES-->
        <section class="sct-certifications-home">
            <div class="container">
                <div class="row justify-content-center">
                    <div class=" wow fadeInUp col-12 content-certifications d-flex justify-content-between align-items-center">
                        <div class="imgs">
                            <img src="assets/images/logos/isoC-9001.jpg" alt="iso-9001">
                            <img src="assets/images/logos/isoC-45001.jpg" alt="iso-45001">
                            <img src="assets/images/logos/isoC-14001.jpg" alt="iso-14001">
                            <img src="assets/images/logos/isoC-37001.jpg" alt="iso-37001">
                            <img class="isoIqnet" src="assets/images/logos/iqnetC.jpg" alt="iqnet">
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <?php
        include 'src/includes/btn-flotant.php'
    ?>
    <?php
        include 'src/includes/footer.php'
    ?>
    
    <script src="assets/js/libraries/flip.js"></script>
    <script src="assets/js/diamonds.js"></script>
    <script src="assets/js/Background.js"></script>
    <script src="assets/js/libraries/fullpage.js"></script>
    <script>
        $(document).ready(function () {
            var iniciar = new acglobalConstructor();
            iniciar.pataAncla();
        });
    </script>
    <!--<script>
        if (screen && screen.width > 1300) {
            let fullpageDiv = $('#fullpage');
            if (fullpageDiv.length) {
                fullpageDiv.fullpage({
                    scrollBar: true,
                    scrollOverflow: true,
                    verticalCentered: true,
                    afterRender: function () {

                    }
                });
            }
        }
    </script>-->
</body>

</html>