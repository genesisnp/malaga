<?php
    include 'src/includes/header.php'
?>
    <main id="report-segment">
        <!--BANNER-->
        <section class="sct-banner">
            <div class="container-fluid">
                <div class="content-img">
                    <img class="img-banner" src="assets/images/canal-de-inquietudes.jpg" alt="img/banner">
                </div>
                <div class="content-title-banner d-flex flex-column">
                    <h1 class="h1-banner text-right">CANAL DE</h1>
                    <h1 class="h1-banner two text-right">INQUIETUDES</h1>
                    <a href="#" class="arrow-repeat no-border d-flex flex-column">
                        <img class="img-arrow-banner" src="assets/images/icons/slim-left.svg" alt="">
                        <!--<p class="p-scroll">scroll</p>-->
                    </a>
                </div>
            </div>
        </section>
        <section class="sct-form-segment">
            <div class="container">
                <div class="row flex-column justify-content-center align-items-center">
                    <div class="col-12 col-lg-6 text-center sg">
                        <i class="icon-seguimiento color-icons wow zoomIn"></i>
                        <h1 class="titles-big title-orange-clear wow zoomIn">SEGUIMIENTO DE REPORTE</h1>
                    </div>
                    <div class="col-12">
                        <div class="wrapper-etapas-seg d-flex">
                            <div class="et-seg text-center d-in-block">
                                <div class="img-seg">
                                    <img src="assets/images/icons/seguimiento-plomo.jpg" alt="">
                                </div>
                                <div class="title-seg">
                                    <p class="p-internas">Denuncia<br>recibida</p>
                                </div>
                            </div>
                            <div class="vacio-seg d-in-block"></div>
                            <div class="et-seg text-center d-in-block">
                                <div class="img-seg">
                                    <img src="assets/images/icons/seguimiento-activo.jpg" alt="">
                                </div>
                                <div class="title-seg">
                                    <p class="p-internas">En proceso de<br>investigación</p>
                                </div>
                            </div>
                            <div class="vacio-seg d-in-block"></div>
                            <div class="et-seg text-center d-in-block">
                                <div class="img-seg">
                                    
                                </div>
                                <div class="title-seg">
                                    <p class="p-internas">Cerrado</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <?php
            include 'src/includes/btn-flotant.php'
        ?>
    </main>
    <?php
        include 'src/includes/footer.php'
    ?>
    <script src="assets/js/libraries/jquery.validate.min.js"></script>
    <script src="assets/js/form-report.js"></script>
</body>

</html>