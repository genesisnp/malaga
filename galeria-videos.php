<?php
    include 'src/includes/header.php'
?>
    <main id="main-news">
        <section class="sct-banner sct-parallax">
            <div class="container-fluid">
                <div class="content-img">
                    <!--<img class="img-banner" src="assets/images/Novedades.jpg" alt="img/banner">-->
                </div>
                <div class="content-title-banner d-flex flex-column">
                    <h1 class="h1-banner">Novedades</h1>
                    <a href="#sct-card-news" data-ancla="sct-card-news" class="arrow-repeat no-border d-flex flex-column">
                        <img class="img-arrow-banner" src="assets/images/icons/slim-left.svg" alt="">
                        <!--<p class="p-scroll">scroll</p>-->
                    </a>
                </div>
            </div>
        </section>

        <section class="sct-card-news" id="sct-card-news" name="sct-card-news">
            <!--SECCION NAVBAR-FIXED NOVEDADES-->
            <div class="container-fluid">
                <div class="row">
                    <?php
                        include 'src/includes/navbar-novedades.php'
                    ?>
                    <div class="col-12 col-lg-9 col-xl-10 sct-parallax info-nav-news">
                        <div class="row mx-0">
                            <?php
                                include 'src/includes/nav-galeria.php'
                            ?>
                            <div class="sct-card-news col-12">
                                <div class="row ">
                                    <div class="col-11 d-flex flex-wrap cnt-divs">
                                        <div class="content-video wow slideInUp">
                                            <div class="card-video">
                                                <a data-fancybox href="https://youtu.be/96F-qXUAclc">
                                                    <img class="card-img-top" src="assets/images/icons/play.svg" />
                                                </a>
                                                <div class="inner-div" style="background-image:url(assets/images/slider-home3.jpg);"></div>
                                            </div>
                                            <div class="card-body">
                                                <p class="card-text">Video Institucional Constructora Málaga</p>
                                            </div>
                                        </div>
                                        <div class="content-video wow slideInUp">
                                            <div class="card-video">
                                                <a data-fancybox href="https://youtu.be/96F-qXUAclc">
                                                    <img class="card-img-top" src="assets/images/icons/play.svg" />
                                                </a>
                                                <div class="inner-div" style="background-image:url(assets/images/slider-home3.jpg);"></div>
                                            </div>
                                            <div class="card-body">
                                                <p class="card-text">Video Institucional Constructora Málaga</p>
                                            </div>
                                        </div>
                                        <div class="content-video wow slideInUp">
                                            <div class="card-video">
                                                <a data-fancybox href="https://youtu.be/96F-qXUAclc">
                                                    <img class="card-img-top" src="assets/images/icons/play.svg" />
                                                </a>
                                                <div class="inner-div" style="background-image:url(assets/images/slider-home3.jpg);"></div>
                                            </div>
                                            <div class="card-body">
                                                <p class="card-text">Video Institucional Constructora Málaga</p>
                                            </div>
                                        </div>
                                        <div class="content-video wow slideInUp">
                                            <div class="card-video">
                                                <a data-fancybox href="https://youtu.be/96F-qXUAclc">
                                                    <img class="card-img-top" src="assets/images/icons/play.svg" />
                                                </a>
                                                <div class="inner-div" style="background-image:url(assets/images/slider-home3.jpg);"></div>
                                            </div>
                                            <div class="card-body">
                                                <p class="card-text">Video Institucional Constructora Málaga</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>          
                        </div>
                    </div>
                </div>
            </div>
        </section>
        
        <?php
            include 'src/includes/btn-flotant.php'
        ?>
    </main>
    <?php
        include 'src/includes/footer.php'
    ?>
    <script src="assets/js/libraries/jquery-fancybox.js"></script>
    <script>
        $(document).ready(function() {
            $("#myvideo").fancybox();
        });
    </script>
</body>

</html>