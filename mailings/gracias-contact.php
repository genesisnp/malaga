<?php
    include 'src/includes/header.php'
?>
    <main id="contact-us">
        <section class="sct-banner sct-parallax">
            <div class="container-fluid">
                <div class="content-img">
                    <!--<img class="img-banner" src="assets/images/contactanos.jpg" alt="img/banner">-->
                </div>
                <div class="content-title-banner d-flex flex-column">
                    <h1 class="h1-banner text-right">CONTÁCTANOS</h1>
                    <a href="#" class="arrow-repeat no-border d-flex flex-column">
                        <img class="img-arrow-banner" src="assets/images/icons/slim-left.svg" alt="">
                        <!--<p class="p-scroll">scroll</p>-->
                    </a>
                </div>
            </div>
        </section>
        <section class="sct-content-thanks sct-parallax">
            <div class="container">
                <div class="row">
                    <div class="col-12 content-title-description text-center text-thanks">
                        <h1 class="titles-big">GRACIAS POR <span class="title-orange-clear">CONTACTARNOS</span></h1>
                        <p class="p-internas">Tu mensaje fue enviado exitosamente.</p>
                        <a href="contactanos.php" class="btn-volver">Volver</a>
                    </div>
                </div>
            </div>
        </section>

        <?php
            include 'src/includes/btn-flotant.php'
        ?>
    </main>
    <?php
        include 'src/includes/footer.php'
    ?>
    <script src="assets/js/libraries/jquery.validate.min.js"></script>
    <script src="assets/js/form-contactUs.js"></script>
</body>

</html>