<?php
    include 'src/includes/header.php'
?>
    <main id="report-segment">
        <!--BANNER-->
        <section class="sct-banner">
            <div class="container-fluid">
                <div class="content-img">
                    <img class="img-banner" src="assets/images/canal-de-inquietudes.jpg" alt="img/banner">
                </div>
                <div class="content-title-banner d-flex flex-column">
                    <h1 class="h1-banner text-right">CANAL DE</h1>
                    <h1 class="h1-banner two text-right">INQUIETUDES</h1>
                    <a href="#" class="arrow-repeat no-border d-flex flex-column">
                        <img class="img-arrow-banner" src="assets/images/icons/slim-left.svg" alt="">
                        <!--<p class="p-scroll">scroll</p>-->
                    </a>
                </div>
            </div>
        </section>
        <section class="sct-report-sent">
            <div class="container">
                <div class="row flex-column justify-content-center align-items-center">
                    <i class="icon-reg-report color-icons"></i>
                    <h1 class="titles-big title-orange-clear">Gracias por su colaboración</h1>
                    <div class="col-6 text-center">
                        <p class="p-internas">Hemos recibido su denuncia, procederemos de acuerdo a los lineamientos
                            internos de la empresa.</p>
                        <p class="p-internas">El número de reporte asignado es : <br><span class="number-asig-report text-center title-blue">487794701</span></p>
                    </div>
                    <div class="col-12 d-flex justify-content-center content-btn-send">
                        <a href="#" type="submit" class="btn btn-send d-flex justify-content-center align-items-center">Regresar</a>
                    </div>
                </div>
            </div>
        </section>
        <?php
            include 'src/includes/btn-flotant.php'
        ?>
    </main>
    <?php
        include 'src/includes/footer.php'
    ?>
</body>

</html>