<!--<div class="col-12 d-flex align-items-center content-titles-gallery">
    <div class="overflow-hide">
        <div class="hole"></div>
        <div class="button "><a class="a-title-gallery" href="galeria-fotos.php">Fotos</a></div>
    </div>
    <div class="overflow-hide">
        <div class="hole"></div>
        <div class="button "><a class="a-title-gallery" href="galeria-videos.php">Videos</a></div>
    </div>
</div>-->
<div class="col-12 d-flex align-items-center content-titles-gallery">
    <ul class="list-nav-gallery d-flex">
        <li class="item-nav-gallery <?= in_array('galeria-fotos.php', $uriSegments ) ? 'active' : ''; ?>"><a class="titles-big" href="galeria-fotos.php">Fotos</a></li>
        <li class="item-nav-gallery <?= in_array('galeria-videos.php', $uriSegments ) ? 'active' : ''; ?>"><a class="titles-big" href="galeria-videos.php">Videos</a></li>
    </ul>
</div>