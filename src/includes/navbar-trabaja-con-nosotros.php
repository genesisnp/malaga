<!--SECCION NAVBAR-FIXED PROYECTOS-->
<div class="sct-navbar-proyect col-12 col-lg-4 col-xl-3">
    <ul class="nav-proyects">
        <li class="item-proyect d-flex justify-content-center align-items-center <?= in_array('nuevo-postulante.php', $uriSegments ) ? 'active' : ''; ?>">
            <a class="link-proyect d-flex flex-column align-items-center text-center"
                href="nuevo-postulante.php">
                <i class="icon-nav icon-postulante"></i>
                <span class="title-nav-proyect">Nuevo<br>Postulante</span>
            </a>
        </li>
        <li class="item-proyect d-flex justify-content-center align-items-center <?= in_array('nuevo-proveedor.php', $uriSegments ) ? 'active' : ''; ?>">
            <a class="link-proyect d-flex flex-column align-items-center text-center"
                href="nuevo-proveedor.php">
                <i class="icon-nav icon-proveedor"></i>
                <span class="title-nav-proyect">Nuevo<br>Proveedor</span>
            </a>
        </li>
    </ul>
</div>