<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="assets/images/logos/favicon.ico" rel="shortcut icon" />
    <title>CONSTRUCTORA MALAGA</title>
    <link rel="stylesheet" href="assets/css/app.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
    <!--[if lte IE 9]>
      <link href='/PATH/TO/FOLDER/css/animations-ie-fix.css' rel='stylesheet'>
    <![endif]-->
</head>

<body>
    <?php 
        $uriSegments = explode("/", parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH));    
    ?>
    <header id="header" name="header">
        <div class="container">
            <div class="row justify-content-lg-between">
                <i id="button-menu" class="icon-hamburger"></i>
                <div class="content-img-logo">
                    <a href="index.php"><img class="img-logo" src="assets/images/logos/logo-cmalaga.svg" alt="logo/malaga"></a>
                </div>
                <div class="content-menus-general">
                    <div class="content-menus d-flex">
                        <div class="content-navTop">
                            <ul class="navTop d-flex">
                                <li class="navTop-item <?= (in_array('nuevo-postulante.php', $uriSegments )
                                                            or in_array('nuevo-proveedor.php', $uriSegments )) 
                                                            ? 'active' : ''; ?>"><a class="navTop-link" href="nuevo-postulante.php">TRABAJA CON NOSOTROS</a></li>

                                <li class="navTop-item <?= in_array('contactanos.php', $uriSegments ) ? 'active' : ''; ?>"><a class="navTop-link" href="contactanos.php">CONTÁCTANOS</a></li>
                                
                                <li class="navTop-item <?= (in_array('canal-de-inquietudes.php', $uriSegments )
                                                        or in_array('registrar-reporte.php', $uriSegments )
                                                        or in_array('seguimiento-de-reporte.php', $uriSegments )
                                                        ) ? 'active' : ''; ?>"><a class="navTop-link" href="canal-de-inquietudes.php">CANAL DE INQUIETUDES</a></li>
                            </ul>
                            <div class="wrapper-search d-flex justify-content-end">
                                <form action="#" class="form-search">
                                    <div class="content-search d-flex align-items-center">
                                        <button class="icon-search"></button>
                                        <input class="input-search" type="search">
                                    </div>
                                </form>
                            </div>
                        </div>
                        <nav class="content-navbar">
                            <ul class="navbar-principal d-flex justify-content-between align-items-xl-center">
                                <li class="item-navbar <?= in_array('nosotros.php', $uriSegments ) ? 'active' : ''; ?>"><a class="link-navbar" href="nosotros.php">NOSOTROS</a></li>
                                <li class="item-navbar sub <?= (in_array('lineas-de-negocio.php', $uriSegments )
                                                        or in_array('edificaciones-publicas-y privadas.php', $uriSegments) 
                                                        or in_array('infraestructura-vial.php', $uriSegments )
                                                        or in_array('saneamiento-e-infraestructura-de-riego.php', $uriSegments ) 
                                                        or in_array('mineria.php', $uriSegments ))? 'active' : ''; ?>">
                                    <a href="lineas-de-negocio.php" class="link-navbar">LÍNEAS DE NEGOCIO</a><span class="d-block d-lg-none icon-arrow-link icon-sub"></span>
                                    <ul class="submenu">
                                        <h1 class="title-submenu d-block d-lg-none"><span class="icon-arrow-link"></span>LÍNEAS DE NEGOCIO</h1>
                                        <li class="item-submenu"><a class="link-submenu l-one" href="mineria.php"><i class="icon-diamonds"></i> Edificaciónes públicas y privadas</a></li>
                                        <li class="item-submenu"><a class="link-submenu" href="mineria.php"><i class="icon-diamonds"></i> Infraestructura vial y aeroportuaria</a></li>
                                        <li class="item-submenu"><a class="link-submenu" href="mineria.php"><i class="icon-diamonds"></i> Saneamiento e Infraestructura de riego</a></li>
                                        <li class="item-submenu"><a class="link-submenu l-four" href="mineria.php"><i class="icon-diamonds"></i> Minería</a></li>
                                    </ul>
                                </li>
                                <li class="item-navbar <?= (in_array('proyectos-edificaciones.php', $uriSegments )
                                                            or in_array('proyectos-infraestructura-vial.php', $uriSegments )
                                                            or in_array('proyectos-saneamiento-e-infraestructura-de-riego.php', $uriSegments )
                                                            or in_array('proyectos-mineria.php', $uriSegments ))
                                                            ? 'active' : ''; ?>"><a class="link-navbar" href="proyectos-mineria.php">PROYECTOS</a></li>
                                <li class="item-navbar rse sub2 <?= (in_array('rse-1.php', $uriSegments )
                                                            or in_array('rse-2.php', $uriSegments )
                                                            or in_array('rse-3.php', $uriSegments ))
                                                            ? 'active' : ''; ?>"><a class="link-navbar" href="rse-1.php">RSE</a></li>
                                <li class="item-navbar <?= (in_array('novedades.php', $uriSegments ) 
                                                        or in_array('galeria-fotos.php', $uriSegments )
                                                        or in_array('galeria-videos.php', $uriSegments ))? 'active' : ''; ?>"><a class="link-navbar" href="novedades.php">NOVEDADES</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </header>