<!--FOOTER-->
<div id="footer" class="section fp-auto-height">
    <footer>
        <div class="container">
            <div class="row align-items-center">
                <div class="col-12 col-md-4">
                    <p class="m-0 company-f"><a class="link-data" href="index.php">CONSTRUCTORA MALAGA</a></p>
                    <p class="m-0 link-data d-none d-md-block">TODOS LOS DERECHOS RESERVADOS 2019</p>
                </div>
                <div class="col-12 col-md-4 text-center">
                    <a href="#" class="icon-rs icon-facebook"></a>
                    <a href="#" class="icon-rs icon-youtube"></a>
                </div>
                <div class="col-12 col-md-4 text-right content-credits">
                    <a class="link-data" href="http://exe.pe/"><span>POWERED BY</span> EXE.PE</a>

                    <p class="m-0 d-flex align-items-center justify-content-center justify-content-md-end">
                        <a class="link-data" href="https://validator.w3.org/check?uri=referer"
                            target="_blank">HTML</a>
                        •
                        <a class="link-data" href="https://jigsaw.w3.org/css-validator/check/referer"
                            target="_blank">CSS</a>
                    </p>
                </div>
            </div>
        </div>
    </footer>
</div>
<script src="assets/js/app.js"></script>
<script src="assets/js/libraries/wow.min.js"></script>
<script>
    var wow = new WOW({
        boxClass:     'wow',      // animated element css class (default is wow)
        animateClass: 'animated', // animation css class (default is animated)
        offset:       0,          // distance to the element when triggering the animation (default is 0)
        mobile:       false,       // trigger animations on mobile devices (default is true)
        live:         true,       // act on asynchronously loaded content (default is true)
        callback:     function(box) {
        // the callback is fired every time an animation is started
        // the argument that is passed in is the DOM node being animated
        },
            scrollContainer: null,    // optional scroll container selector, otherwise use window,
            resetAnimation: true,     // reset animation on end (default is true)
        }
    );
    wow.init();
</script>
