<div class="sct-navbar-news col-12 col-lg-3 col-xl-2 px-0 zIndex-0 wow slideInLeft" id="navbar-news">
    <ul class="nav-proyects">
        <li class="item-proyect d-flex justify-content-center align-items-center <?= in_array('novedades.php', $uriSegments ) ? 'active' : ''; ?>">
            <a class="link-proyect d-flex flex-column align-items-center text-center"
                href="novedades.php">
                <i class="icon-nav icon-novedades"></i>
                <span class="title-nav-proyect">Novedades</span>
            </a>
        </li>
        <li class="item-proyect d-flex justify-content-center align-items-center <?= (in_array('galeria-fotos.php', $uriSegments ) or in_array('galeria-videos.php', $uriSegments )) ? 'active' : ''; ?>">
            <a class="link-proyect d-flex flex-column align-items-center text-center"
                href="galeria-fotos.php">
                <i class="icon-nav icon-proveedor"></i>
                <span class="title-nav-proyect">Galería<br>Fotos y Videos</span>
            </a>
        </li>
    </ul>
</div>