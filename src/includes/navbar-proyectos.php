<div class="sct-navbar-proyect col-12 col-sm-9 col-md-6 col-lg-3 col-xl-2 px-0 wow slideInLeft">
    <div class="navbar-proyect-mobile" id="navbar-proyect-mobile">
        <h1 class="title-navbar-mobile">Selecciona un Rubro <i class="icon-arrow-link"></i></h1>
    </div>
    <ul class="nav-proyects" id="nav-proyects">
        <li class="item-proyect d-flex justify-content-start justify-content-lg-center align-items-center <?= in_array('proyectos-obras-emblematicas.php', $uriSegments ) ? 'active' : ''; ?>">
            <a class="link-proyect d-flex flex-column align-items-lg-center text-center" href="proyectos-mineria.php">
                <i class="icon-nav icon-emblema"></i>
                <span class="title-nav-proyect">Obras <br class="d-none d-lg-block">Emblemáticas</span>
            </a>
        </li>
        <li class="item-proyect d-flex justify-content-start justify-content-lg-center align-items-center <?= in_array('proyectos-edificaciones.php', $uriSegments ) ? 'active' : ''; ?>">
            <a class="link-proyect d-flex flex-column align-items-lg-center text-center" href="proyectos-mineria.php">
                <i class="icon-nav icon-edificacion"></i>
                <span class="title-nav-proyect">Edificaciones <br class="d-none d-lg-block">Publicas y Privadas</span>
            </a>
        </li>
        <li class="item-proyect d-flex justify-content-start justify-content-lg-center align-items-center <?= in_array('proyectos-infraestructura-vial.php', $uriSegments ) ? 'active' : ''; ?>">
            <a class="link-proyect d-flex flex-column align-items-lg-center text-center" href="proyectos-mineria.php">
                <i class="icon-nav icon-vial-det"></i>
                <span class="title-nav-proyect">Infraestructura vial<br class="d-none d-lg-block"> y aeroportuaria</span>
            </a>
        </li>
        <li class="item-proyect d-flex justify-content-start justify-content-lg-center align-items-center <?= in_array('proyectos-saneamiento-e-infraestructura-de-riego.php', $uriSegments ) ? 'active' : ''; ?>">
            <a class="link-proyect d-flex flex-column align-items-lg-center text-center" href="proyectos-mineria.php">
                <i class="icon-nav icon-saneamiento"></i>
                <span class="title-nav-proyect">Saneamiento<br class="d-none d-lg-block"> e infraestructura <br class="d-none d-lg-block">de riego</span>
            </a>
        </li>
        <li class="item-proyect d-flex justify-content-start justify-content-md-center align-items-center <?= in_array('proyectos-mineria.php', $uriSegments ) ? 'active' : ''; ?>">
            <a class="link-proyect d-flex flex-column align-items-lg-center text-center" href="proyectos-mineria.php">
                <i class="icon-nav icon-mineria"></i>
                <span class="title-nav-proyect">Minería</span>
            </a>
        </li>
    </ul>
</div>