<section class="sct-category-products sct-arrow section" id="sct-category-products" name="sct-category-products">
    <div id="inicio_servicios" class="contenedor total10 d-flex justify-content-center align-items-center">
        <div id="content-diamantes" class="fondo perspectiva10 vista00001 anim presto opacidad matriz escala12 demora5">
            <div class="fondo ancla_contenedor grupo0 indice0 pata_activo ">
                <div class="fondo_imagen blur-bg" style="background-image:url(assets/images/diamond/edificaciones-publicas.jpg)"></div>
                <div class="bg-sct-two"></div>
            </div>
            <div class="fondo ancla_contenedor grupo0 indice1 ">
                <div class="fondo_imagen blur-bg" style="background-image:url(assets/images/diamond/infraestructura-vial.jpg)"></div>
                <div class="bg-sct-two"></div>
            </div>
            <div class="fondo ancla_contenedor grupo0 indice2 ">
                <div class="fondo_imagen blur-bg" style="background-image:url(assets/images/diamond/saneamiento.jpg)"></div>
                <div class="bg-sct-two"></div>
            </div>
            <div class="fondo ancla_contenedor grupo0 indice3 ">
                <div class="fondo_imagen blur-bg" style="background-image:url(assets/images/diamond/mineria.jpg)"></div>
                <div class="bg-sct-two"></div>
            </div>
        </div>
        <div class="container-fluid z-ind anim presto matriz escala20 demora5">
            <div class="row justify-content-center align-items-start curva anim presto" data-sequence='500' data-pausa="200">
                <div class="col-12 title-sct-prd text-center ">
                    <h1 class="title_sct_two wow zoomIn">líneas de 
                        <span class="title-orange-clear">Negocio</span>
                    </h1>
                </div>
                <div class="wrapper-diamonds col-12 d-flex justify-content-center align-items-center wow slideInUp">
                    <div class="position-relative d-flex justify-content-center align-items-center flex-column flex-lg-row">
                        <a id="neg-uno" href="mineria.php" class="columnita d-flex align-items-center flex-column bloque alinea izquierda_centro ancla_pata pata_activo" data-grupo="0" data-indice="0" data-flota>
                            <div class="containers">
                                <div class="front" style="background-color: #009fe4">
                                    <div class="inner">
                                        <p class="title-flip">EDIFICACIONES<br>PÚBLICAS Y PRIVADAS</p>                                    
                                    </div>
                                </div>
                                <div class="back" style="background-image: url(assets/images/diamond/edificaciones-publicas.jpg)">
                                    <div class="inner">
                                        <p class="title-flip">EDIFICACIONES<br>PÚBLICAS Y PRIVADAS</p>
                                        <div class="arrow-diamonds"><img src="assets/images/icons/slim-right.svg" alt=""></div>
                                    </div>
                                </div>
                            </div>
                        </a>                        
                        <a id="neg-dos" href="mineria.php"class="columnita d-flex align-items-center flex-column bloque alinea izquierda_centro ancla_pata " data-grupo="0" data-indice="1" data-flota>
                            <div class="containers">
                                <div class="front" style=" background-color: #143a7d">
                                    <div class="inner">
                                        <p class="title-flip">INFRAESTRUCTURA VIAL<br>Y AEROPORTUARIA</p>
                                    </div>
                                </div>
                                <div class="back" style="background-image: url(assets/images/diamond/infraestructura-vial.jpg)">
                                    <div class="inner">
                                        <p class="title-flip">INFRAESTRUCTURA VIAL<br>Y AEROPORTUARIA</p>
                                        <div class="arrow-diamonds"><img src="assets/images/icons/slim-right.svg" alt=""></div>
                                    </div>
                                </div>
                            </div>
                        </a>
                        <a id="neg-tres" href="mineria.php" class="columnita d-flex align-items-center flex-column bloque alinea izquierda_centro ancla_pata " data-grupo="0" data-indice="2" data-flota>
                            <div class="containers">
                                <div class="front" style="background-color: #e94e1b">
                                    <div class="inner">
                                        <p class="title-flip">SANEAMIENTO<br>E INFRAESTRUCTURA<br>DE RIEGO</p>
                                    </div>
                                </div>
                                <div class="back" style="background-image: url(assets/images/diamond/saneamiento.jpg)">
                                    <div class="inner">
                                        <p class="title-flip">SANEAMIENTO<br>E INFRAESTRUCTURA<br>DE RIEGO</p>
                                        <div class="arrow-diamonds"><img src="assets/images/icons/slim-right.svg" alt=""></div>
                                    </div>
                                </div>
                            </div>
                        </a>
                        <a id="neg-cuatro" href="mineria.php" class="columnita d-flex align-items-center flex-column bloque alinea izquierda_centro ancla_pata " data-grupo="0" data-indice="3" data-flota>
                            <div class="containers">
                                <div class="front" style="background-color: #f39100">
                                    <div class="inner">
                                        <p class="title-flip">MINERÍA</p>
                                    </div>
                                </div>
                                <div class="back" style="background-image: url(assets/images/diamond/mineria.jpg)">
                                    <div class="inner">
                                        <p class="title-flip">MINERÍA</p>
                                        <div class="arrow-diamonds"><img src="assets/images/icons/slim-right.svg" alt=""></div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>

                </div>
            </div>
        </div>
        <div class="f-black"></div>
    </div>
</section>
