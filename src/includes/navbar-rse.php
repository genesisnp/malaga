<div class="sct-navbar-proyect col-12 col-lg-3 col-xl-2 px-0 wow slideInLeft" id="navbar-rse">
    <ul class="nav-proyects">
        <li class="item-proyect d-flex justify-content-center align-items-center <?= in_array('rse-1.php', $uriSegments ) ? 'active' : ''; ?>">
            <a class="link-proyect d-flex flex-column align-items-center text-center"
                href="rse-1.php">
                <i class="icon-nav icon-novedades"></i>
                <span class="title-nav-proyect">Rse - 1</span>
            </a>
        </li>
        <li class="item-proyect d-flex justify-content-center align-items-center <?= in_array('rse-2.php', $uriSegments ) ? 'active' : ''; ?>">
            <a class="link-proyect d-flex flex-column align-items-center text-center"
                href="rse-2.php">
                <i class="icon-nav icon-novedades"></i>
                <span class="title-nav-proyect">Rse - 2</span>
            </a>
        </li>
        <li class="item-proyect d-flex justify-content-center align-items-center <?= in_array('rse-3.php', $uriSegments ) ? 'active' : ''; ?>">
            <a class="link-proyect d-flex flex-column align-items-center text-center"
                href="rse-3.php">
                <i class="icon-nav icon-novedades"></i>
                <span class="title-nav-proyect">Rse - 3</span>
            </a>
        </li>
    </ul>
</div>