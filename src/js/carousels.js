$(document).ready(function () {
    //CAROUSEL SECCION ONE HOME
    $('#owl-carousel-principal').owlCarousel({
        item:1,
        loop: true,
        //margin:10,
        animateOut: 'scale-carousel',
        nav: true,
        autoplay:true,
        navText: [
            '<img class="img-arrow-home" src="assets/images/icons/arrow-next-home.svg">',
            '<img class="img-arrow-home" src="assets/images/icons/arrow-prev-home.svg">'
        ],
        responsive:{
            0:{
                items:1
            },
            600:{
                items:1
            },
            1000:{
                items:1
            }
        }
        
    })

    //CAROUSEL DE PROYECTS SECCION THREE DEL HOME
    var sync1 = $("#carousel-img-home");
    var sync2 = $("#carousel-home-info");
    var syncedSecondary = true;

    sync1.owlCarousel({
        items: 1,
        slideSpeed: 3000,
        nav: true,
        autoplay: true,
        dots: true,
        loop: true,
        animateOut: 'scale-carousel',
        onInitialized: counter,
        onTranslated: counter,
        responsiveRefreshRate: 200,
        responsive:{
            900:{
                items:1,
                margin:10
            },
            1025:{
                navText: [
                    '<img src="assets/images/icons/slim-left.svg">',
                    '<img src="assets/images/icons/slim-right.svg">'
                ]
            }
        }
    }).on('changed.owl.carousel', syncPosition);

    sync2
        .on('initialized.owl.carousel', function () {
            sync2.find(".owl-item").eq(0).addClass("current");
        })
        .owlCarousel({
            items: 1,
            dots: true,
            //nav: true,
            onInitialized: counter,
            onTranslated: counter,
            //animateOut: 'scale-carousel',
            //smartSpeed: 200,
            //slideSpeed: 500,
            //slideBy: 2,
            //responsiveRefreshRate: 100,
            animateOut: 'scale-carousel',
            900:{
                items:1
            }
        }).on('changed.owl.carousel', syncPosition2);

    function syncPosition(el) {
        var count = el.item.count - 1;
        var current = Math.round(el.item.index - (el.item.count / 2) - .5);

        if (current < 0) {
            current = count;
        }
        if (current > count) {
            current = 0;
        }

        sync2
            .find(".owl-item")
            .removeClass("current")
            .eq(current)
            .addClass("current");
        var onscreen = sync2.find('.owl-item.active').length - 1;
        var start = sync2.find('.owl-item.active').first().index();
        var end = sync2.find('.owl-item.active').last().index();

        if (current > end) {
            sync2.data('owl.carousel').to(current, 100, true);
        }
        if (current < start) {
            sync2.data('owl.carousel').to(current - onscreen, 100, true);
        }
    }

    function syncPosition2(el) {
        if (syncedSecondary) {
            var number = el.item.index;
            sync1.data('owl.carousel').to(number, 100, true);
        }
    }

    sync2.on("click", ".owl-item", function (e) {
        e.preventDefault();
        var number = $(this).index();
        sync1.data('owl.carousel').to(number, 500, true);
    });
    //FUNCION PARA MOSTRAR POSICION(number) EN EL CAROUSEL
    function counter(event) {
        var element = event.target;
        var items = event.item.count;
        var item = event.item.index + 1;

        if (item > items) {
            item = item - items
        }
        $('.counter .item-actual').html(item)
        $('.counter .slash').html('/')
        $('.counter .total-items').html(items)
    }
    /*INICIALIZACIÓN DE CAROUSEL PROYECTS*/
    $('.carousel-img-repeat').owlCarousel({
        items: 1,
        slideSpeed: 2000,
        nav: true,
        autoplay: true,
        dots: true,
        loop: true,
        onInitialized: counter,
        onTranslated: counter,
        responsiveRefreshRate: 200,
        animateOut: 'scale-carousel',
        responsive:{
            1025:{
                navText: [
                    '<img src="assets/images/icons/slim-left.svg">',
                    '<img src="assets/images/icons/slim-right.svg">'
                ]
            }
        }
    }).on('changed.owl.carousel');

});