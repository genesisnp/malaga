$(document).ready(function () {
    // MOSTRANDO Y OCULTANDO MENU
    if (window.innerWidth <= 1024) {
        $('#button-menu').click(function () {
            if ($('#button-menu').attr('class') == 'icon-hamburger') {

                $('#button-menu').removeClass('icon-hamburger').addClass('icon-back');
                $('.content-menus-general .content-menus').css({
                    'left': '0%'
                });

            } else {

                $('#button-menu').removeClass('icon-back').addClass('icon-hamburger');

                $('.content-menus-general .content-menus').css({
                    'left': '-90%'
                }); // Ocultamos el Menu

            }
        });
        // MOSTRANDO SUBMENU
        $('.navbar-principal >.item-navbar.sub .icon-sub').click(function () {
            $('.navbar-principal >.item-navbar.sub > .submenu').css({
                'right': '0'
            });
            $('#button-menu').removeClass('icon-back')
        });

        // OCULTANDO SUBMENU
        $('.navbar-principal .submenu .icon-arrow-link').click(function () {

            $('.navbar-principal .item-navbar > .submenu').css({
                'right': '-100%'
            });
            $('#button-menu').addClass('icon-back')

        });
    }
});