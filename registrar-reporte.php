<?php
    include 'src/includes/header.php'
?>
    <main class="main-report">
        <div class="container">
            <div class="row">
                <div class="col-12 d-flex align-items-center">
                    <a href="canal-de-inquietudes.php" class="icon-arrow-link color-icons"></a>
                    <div class="d-flex align-items-center">
                        <i class="icon-report icon-reg-report color-icons"></i>
                        <h1 class="titles-report title-orange-clear">Registrar Reporte</h1>
                    </div>
                </div>
                <div class="col-12 info-regist content-form">
                    <form action="#" class="row form-reg-report">
                        <!--REGISTRO-->
                        <h1 class="title-reg col-12">Registro</h1>
                        <div class="col-12 d-flex swits-content">
                            <div class="row">
                                <span class="p-internas col-1">Anómino</span>
                                <div class='switch'>
                                    <div class='span'>NO</div>
                                </div>
                                <div class="col-12 datos-reg">
                                    <div class="row">
                                        <div class="form__wrapper col-12 col-lg-3">
                                            <input type="text" class="form-bg form__input" id="name-report" name="name-report">
                                            <label class="form__label">
                                                <span class="form__label-content">Nombres:</span>
                                            </label>
                                        </div>
                                        <div class="form__wrapper col-12 col-lg-3">
                                            <input type="text" class="form-bg form__input" id="lastname-report" name="lastname-report">
                                            <label class="form__label">
                                                <span class="form__label-content">Apellidos:</span>
                                            </label>
                                        </div>
                                        <div class="form__wrapper col-12 col-lg-3">
                                            <input type="email" class="form-bg form__input" id="email-report" name="email-report">
                                            <label class="form__label">
                                                <span class="form__label-content">E-mail:</span>
                                            </label>
                                        </div>
                                        <div class="form__wrapper col-12 col-lg-3">
                                            <input type="text" class="form-bg form__input" id="position-report" name="position-report">
                                            <label class="form__label">
                                                <span class="form__label-content">Cargo:</span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                        
                        <!--INGRESAR REPORTE-->
                        <h1 class="title-reg col-12">Ingresar Reporte</h1>
                        <div class="col-12">
                            <div class="row justify-content-between">
                                <div class="col-12 col-lg-6">
                                    <div class="row content-radio-adj">
                                        <div class="col-12 d-flex align-items-center">
                                            <i class="icon-arrow color-icons"></i>
                                            <span class="p-internas">Involucrado N°1</span>
                                            <img src="assets/images/icons/registro-mas.svg" alt="">
                                        </div>
                                        <div class="form-invol row">
                                            <div class="form__wrapper col-12 col-lg-6">
                                                <input type="text" class="form-bg form__input" id="name-involved" name="name-involved">
                                                <label class="form__label">
                                                    <span class="form__label-content">Nombres:</span>
                                                </label>
                                            </div>
                                            <div class="form__wrapper col-12 col-lg-6">
                                                <input type="text" class="form-bg form__input" id="lastname-involved" name="lastname-involved">
                                                <label class="form__label">
                                                    <span class="form__label-content">Apellido:</span>
                                                </label>
                                            </div>
                                            <div class="form__wrapper col-12">
                                                <input type="text" class="form-bg form__input" id="position-involved" name="position-involved">
                                                <label class="form__label">
                                                    <span class="form__label-content">Cargo:</span>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="p-adj-evid d-flex align-items-center col-12">
                                            <i class="icon-arrow color-icons"></i>
                                            <span class="p-internas">Adjuntar evidencia</span>
                                        </div>
                                        <div class="radio col-12">
                                            <input id="radio-1" name="radio" type="radio">
                                            <label for="radio-1" class="radio-label p-internas">No me es posible proporcionar evidencias de
                                                ningún tipo.</label>
                                        </div>
                                        <div class="radio col-12">
                                            <input id="radio-2" name="radio" type="radio">
                                            <label for="radio-2" class="radio-label p-internas">No tengo evidencias, pero podría obtenerlas
                                                y suministrarlas posteriorme.</label>
                                        </div>
                                        <div class="radio col-12">
                                            <input id="radio-3" name="radio" type="radio">
                                            <label for="radio-3" class="radio-label p-internas">Tengo evidencia física y/o digital que me
                                                gustaría entregar.</label>
                                        </div>
                                        <div class="radio col-12">
                                            <input id="radio-4" name="radio" type="radio">
                                            <label for="radio-4" class="radio-label p-internas">Tengo evidencia física que deseo entregar</label>
                                        </div>
                                        <div class="radio col-12">
                                            <input id="radio-5" name="radio" type="radio">
                                            <label for="radio-5" class="radio-label p-internas">Tengo evidencia digital que deseo entregar</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-lg-5">
                                    <div class="row">
                                        <div class="d-flex align-items-center p-descr-incident">
                                            <i class="icon-arrow color-icons"></i>
                                            <span class="p-internas">Descripción del Incidente</span>
                                        </div>
                                        <div class="form__wrapper col-12">
                                            <input type="text" class="form-bg form__input" id="place-incident" name="place-incident">
                                            <label class="form__label">
                                                <span class="form__label-content">Lugar:</span>
                                            </label>
                                        </div>
                                        <div class="form__wrapper col-12 col-lg-6">
                                            <input type="text" class="form-bg form__input" id="date-incident" name="date-incident">
                                            <label class="form__label">
                                                <span class="form__label-content">Fecha:</span>
                                            </label>
                                        </div>
                                        <div class="form__wrapper col-12 col-lg-6">
                                            <input type="text" class="form-bg form__input" id="time-incident" name="time-incident">
                                            <label class="form__label">
                                                <span class="form__label-content">Hora:</span>
                                            </label>
                                        </div>
                                        <div class="form__wrapper col-12">
                                            <textarea class="form-bg form_textarea" id="detail-incident" name="detail-incident" rows="17"></textarea>
                                            <label class="form__label">
                                                <span class="form__label-content">Detalle:</span>
                                            </label>
                                        </div>

                                        <div class="box">
                                            <input type="checkbox" id="box-1">
                                            <label for="box-1" class="p-internas">He leído y acepto los <a href="#" class="title-blue">Términos y condiciones</a></label>
                                        </div>
                                        <div class="form-group col-12 d-flex justify-content-end content-btn-send">
                                            <button type="submit" class="btn btn-send">Enviar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </form>
                </div>
            </div>
        </div>
        <?php
            include 'src/includes/btn-flotant.php'
        ?>
    </main>
    <?php
        include 'src/includes/footer.php'
    ?>
    <script src="assets/js/switch.js"></script>
    <script src="assets/js/libraries/jquery.validate.min.js"></script>
    <script src="assets/js/form-report.js"></script>
</body>

</html>