<?php
    include 'src/includes/header.php'
?>
    <main id="main-line-deal">  
        <?php
            include 'src/includes/diamonds-lineas-negocio.php'
        ?>
        <?php
            include 'src/includes/btn-flotant.php'
        ?>
        <?php
            include 'src/includes/footer.php'
        ?>
    </main>
    <script src="assets/js/libraries/flip.js"></script>
    <script src="assets/js/diamonds.js"></script>
    <script src="assets/js/Background.js"></script>
    <script>
        $(document).ready(function () {
            var iniciar = new acglobalConstructor();
            iniciar.pataAncla();
        });
    </script>
</body>

</html>