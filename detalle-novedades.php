<?php
    include 'src/includes/header.php'
?>
    <main class="main-detail-news">
        <section class="sct_detail_news container">
            <div class="row">
                <h2 class="col-12 text-center titles-big">NOVEDADES</h2>
                <a href="novedades.php" class="icon-icono-regresar text-center col-12"></a>
                <div class="col-12 col-lg-5 px-0 content-img-detail">
                    <div class="owl-carousel owl-img-detail owl-theme">
                        <div class="item">
                            <img src="assets/images/HOSPITAL-ESSALUD-PISCO.jpg" alt="">
                        </div>
                        <div class="item">
                            <img src="assets/images/Carretera-Tocache.jpg" alt="">
                        </div>
                        <div class="item">
                            <img src="assets/images/CARRETERA-JUNIN.jpg" alt="">
                        </div>
                    </div>
                </div>
                <div class="col-12 col-lg-7 description-news-general">
                    <div class="content-title-detail-news">
                        <h1 class="title-blue">Hospital de apoyo de Nazca</h1>
                        <span class="p-date-detail">10 de Octubre de 2018</span>
                        <div class="content-icon-detail"><a href="www.facebook.com" class="icon-detail icon-facebook"></a> <a href="" class="icon-detail icon-twitter"></a></div>
                    </div>
                    <div class="content-description-detail-news">
                        <p class="p-internas">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec blandit laoreet ex a feugiat. Cras vitae dui fringilla, molestie risus condimentum, porta quam. Nulla euismod dictum venenatis. Maecenas id iaculis ante, ac varius libero. Proin iaculis bibendum magna rutrum congue. Maecenas sit amet sodales tortor. Aliquam feugiat blandit augue quis maximus.</p>
                        <p class="p-internas">Sed sed efficitur leo, rutrum suscipit ex. Donec in rutrum leo. Quisque dignissim, nunc at condimentum sagittis, sem mi hendrerit lorem, aliquam auctor nibh ligula ut metus. Sed hendrerit vel justo quis pulvinar. Proin eget metus ac mauris sollicitudin feugiat. Morbi non porttitor diam, sed interdum dolor. Morbi sagittis nisi sed ligula vulputate, quis egestas arcu bibendum. Praesent tempus dui vitae enim tempus tristique. Quisque ligula justo, vestibulum sit amet sapien eget, malesuada fringilla ante. Nunc nec arcu arcu. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                        <p class="p-internas">Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec convallis sed sem ac luctus. Donec convallis enim vitae consequat dignissim. Duis ligula nisi, rutrum a tortor at, pretium dapibus tellus. Quisque vitae laoreet ante, vel porta magna. Donec scelerisque pharetra libero nec congue. Pellentesque ac nunc est. Maecenas id enim dignissim, convallis ante a, bibendum eros. Morbi nec aliquam augue, eu suscipit massa. Etiam ac tellus id sem varius consectetur. Aliquam a placerat nulla, nec dapibus libero. Quisque aliquam odio et aliquet ultrices.</p>
                        <p class="p-internas">Sed sed efficitur leo, rutrum suscipit ex. Donec in rutrum leo. Quisque dignissim, nunc at condimentum sagittis, sem mi hendrerit lorem, aliquam auctor nibh ligula ut metus. Sed hendrerit vel justo quis pulvinar. Proin eget metus ac mauris sollicitudin feugiat. Morbi non porttitor diam, sed interdum dolor. Morbi sagittis nisi sed ligula vulputate, quis egestas arcu bibendum. Praesent tempus dui vitae enim tempus tristique. Quisque ligula justo, vestibulum sit amet sapien eget, malesuada fringilla ante. Nunc nec arcu arcu. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                    </div>
                </div>
            </div>
        </section>
        <?php
            include 'src/includes/btn-flotant.php'
        ?>
    </main>
        
    <?php
        include 'src/includes/footer.php'
    ?>
    <script src="assets/js/detail-news.js"></script>
</body>

</html>