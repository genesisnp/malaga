<?php
    include 'src/includes/header.php'
?>
    <main id="channel-of-concerns">
        <section class="sct-banner sct-parallax">
            <div class="container-fluid">
                <div class="content-img">
                    <!--<img class="img-banner" src="assets/images/canal-de-inquietudes.jpg" alt="img/banner">-->
                </div>
                <div class="content-title-banner d-flex flex-column">
                    <h1 class="h1-banner text-right">CANAL DE</h1>
                    <h1 class="h1-banner two text-right">INQUIETUDES</h1>
                    <a href="#" class="arrow-repeat no-border d-flex flex-column">
                        <img class="img-arrow-banner" src="assets/images/icons/slim-left.svg" alt="">
                        <!--<p class="p-scroll">scroll</p>-->
                    </a>
                </div>
            </div>
        </section>
        <section class="detail-channel-of-concerns sct-parallax">
            <div class="container">
                <div class="row justify-content-center align-items-center">
                    <div class="col-12 col-lg-11 text-center description-ch-of-conc">
                        <div class="d-flex align-items-center flex-column wow zoomIn">
                            <i class="icon-form-nav icon-funciona"></i>
                            <h1 class="titles-big title-orange-clear">¿CÓMO FUNCIONA?</h1>
                        </div>
                        <p class="p-internas">Es un canal donde los colaboradores, socios de negocios y terceros podrán
                            informar presuntas irregularidades de carácter general, operativo o financiero y/o
                            infracciones a la empresa, sin ningún miedo a represalias por haber efectuado una denuncia.
                        </p>
                        <p class="p-internas">Se recibe, registra y clasifica de modo confidencial las denuncias
                            recibidas, luego son remitidas al Comité responsable de su evaluación, se puede realizar
                            denuncias anónimas.</p>
                    </div>
                    <div class="col-12 ">
                        <div class="row">
                            <h2 class="title-big-rep col-12 text-center wow zoomIn">FORMULARIO WEB</h2>
                            <div class="col-12 px-0">
                                <p class="p-internas text-center">Denuncia la irregularidad a través de nuestra
                                        plataforma:</p>
                                <ul class="navbar-form-web d-flex justify-content-between justify-content-lg-center">
                                    <li class="item-form-web wow zoomIn">
                                        <a class="link-form-web d-flex flex-column align-items-center justify-content-center"
                                            href="registrar-reporte.php"><i class="icon-form-web icon-reg-report"></i><span
                                                class="titles-big">REGISTRAR REPORTE</span></a>
                                    </li>

                                    <li class="item-form-web wow zoomIn">
                                        <a class="link-form-web d-flex flex-column align-items-center justify-content-center"
                                            href="seguimiento-de-reporte.php"><i class="icon-form-web icon-seg-report"></i><span
                                                class="titles-big">SEGUIMIENTO DE
                                                REPORTE<span></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 px-0">
                        <div class="row rep-irrg">
                            <h2 class="title-big-rep col-12 text-center">¿CÓMO PUEDO REPORTAR LA IRREGULARIDAD?</h2>
                            <p class="p-internas text-center col-12">Puedes reportar la irregularidad a travéz de los siguientes canales de denuncia:</p>
                            <div class="col-10 col-md-6 col-lg-3 d-flex flex-column align-items-center wow zoomIn">
                                <p class="title-rep-irr d-flex flex-column align-items-center"><i
                                        class="icon-direccion-postal icon-rep-irr"></i><span>Dirección Postal</span></p>
                                <div class="content-info-rep">
                                    <p class="p-internas">Puedes remitir la irregularidad, mediante sobre
                                        cerrado, a las oficinas de Constructora Malaga Hrnos. S.A. en:</p>
                                    <p class="p-internas"><i class="icon-diamonds color-icons"></i>Av.
                                        Manuel Olguín 211 - Piso 1702<br>Santiago de
                                        Surco - Lima</p>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 col-lg-3 d-flex flex-column align-items-center wow zoomIn" data-wow-delay="1s">
                                <p class="title-rep-irr d-flex flex-column align-items-center"><i class="icon-destinatario icon-rep-irr"></i><span>Destinatario</span></p>
                                <div class="content-info-rep">
                                    <p class="p-internas">Canal de Cumplimiento.</p>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 col-lg-3 d-flex flex-column align-items-center wow zoomIn" data-wow-delay="2s">
                                <p class="title-rep-irr d-flex flex-column align-items-center"><i class="icon-sobre icon-rep-irr"></i><span>Buzón
                                        de correo</span></p>
                                <div class="content-info-rep">
                                    <p class="p-internas">Denuncia la irregularidad al siguiente
                                        e-mail:<br><a href="mailto:cumplimiento@c-malaga.com"><strong class="title-blue">cumplimiento@c-malaga.com</strong></a></p>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 col-lg-3 d-flex flex-column align-items-center wow zoomIn" data-wow-delay="3s">
                                <p class="title-rep-irr d-flex flex-column align-items-center"><i
                                        class="icon-wts icon-rep-irr"></i><span>Whatsapp</span></p>
                                <div class="content-info-rep">
                                    <p class="p-internas"><a class="title-blue" href="#">(511) 949063207</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <?php
            include 'src/includes/btn-flotant.php'
        ?>
    </main>
    <?php
        include 'src/includes/footer.php'
    ?>

</body>

</html>