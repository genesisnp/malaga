
<?php
    include 'src/includes/header.php'
?>
    <main class="work-with-us">
        <section class="sct-banner sct-parallax">
            <div class="container-fluid">
                <div class="content-img">
                    <!--<img class="img-banner" src="assets/images/trabaja-con-nostros.jpg" alt="img/banner">-->
                </div>
                <div class="content-title-banner d-flex flex-column">
                    <h1 class="h1-banner text-right">TRABAJA CON<br>NOSOTROS</h1>
                    <a href="#" class="arrow-repeat no-border d-flex flex-column">
                        <img class="img-arrow-banner" src="assets/images/icons/slim-left.svg" alt="">
                        <!--<p class="p-scroll">scroll</p>-->
                    </a>
                </div>
            </div>
        </section>
        <section class="form-work-with-us sct-parallax">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-12 col-xl-10">
                        <div class="row">
                            <!--SECCION NAVBAR-FIXED PROYECTOS-->
                            <?php
                                include 'src/includes/navbar-trabaja-con-nosotros.php'
                            ?>

                            <div class="content-form col-12 col-lg-8 col-xl-9 wow slideInRight">
                                <div class="title-form d-flex align-items-center">
                                    <i class="icon-form-nav icon-postulante"></i>
                                    <h1 class="titles-big">Nuevo<br><span class="title-orange-clear">POSTULANTE</span>
                                    </h1>
                                </div>
                                <form action="#" class="form row" method="post" id="form-new-applicant">
                                    <div class="form__wrapper col-12 col-lg-6">
                                        <input type="text" class="form-bg form__input" id="name-postulant" name="name-postulant">
                                        <label class="form__label">
                                            <span class="form__label-content">Nombres:</span>
                                        </label>
                                    </div>
                                    <div class="form__wrapper col-12 col-lg-6">
                                        <input type="text" class="form-bg form__input" id="lastName-postulant" name="lastName-postulant">
                                        <label class="form__label">
                                            <span class="form__label-content">Apellidos:</span>
                                        </label>
                                    </div>
                                    <div class="form__wrapper col-12 col-lg-6">
                                        <input type="text" class="form-bg form__input" id="phone-postulant" name="phone-postulant" maxlength="9" onkeypress="if (event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;">
                                        <label class="form__label">
                                            <span class="form__label-content">Teléfono:</span>
                                        </label>
                                    </div>
                                    <div class="form__wrapper col-12 col-lg-6">
                                        <input type="text" class=" form-bg form__input" id="dni-postulant" name="dni-postulant" maxlength="8" onkeypress="if (event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;">
                                        <label class="form__label">
                                            <span class="form__label-content">Dni:</span>
                                        </label>
                                    </div>
                                    <div class="form__wrapper col-12 col-lg-6">
                                        <input type="email" class="form-bg form__input" id="email-postulant" name="email-postulant">
                                        <label class="form__label">
                                            <span class="form__label-content">Email:</span>
                                        </label>
                                    </div>
                                    <div class="select form__wrapper col-12 col-lg-6">
                                        <select>
                                            <option>Puesto</option>
                                            <option>puesto 1</option>
                                            <option>puesto2</option>
                                        </select>
                                        <div class="icon-arrow-link"></div>
                                    </div>
                                    <div class="col-12">
                                        <label for="file" class="form__label">Adjunta CV</label> 
                                        <input type="text" class="form-control input-bg-file">
                                        <input type="file" name="upload" id="file" class="form-control">
                                        <img class="img-file" src="assets/images/icons/download-button.svg" alt="">
                                    </div>
                                    
                                    <div class="form-group col-12 d-flex justify-content-center justify-content-lg-end content-btn-send">
                                        <button type="submit" class="btn btn-send" id="btn-newApplicant">Enviar</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <?php
            include 'src/includes/btn-flotant.php'
        ?>
    </main>
    <?php
        include 'src/includes/footer.php'
    ?>
    <script src="assets/js/libraries/jquery.validate.min.js"></script>
    <script src="assets/js/form-postulante.js"></script>
</body>

</html>