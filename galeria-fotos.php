<?php
    include 'src/includes/header.php'
?>
    <main id="main-news">
        <section class="sct-banner sct-parallax">
            <div class="container-fluid">
                <div class="content-img">
                    <!--<img class="img-banner" src="assets/images/Novedades.jpg" alt="img/banner">-->
                </div>
                <div class="content-title-banner d-flex flex-column">
                    <h1 class="h1-banner">Novedades</h1>
                    <a href="#sct-card-news" data-ancla="sct-card-news" class="arrow-repeat no-border d-flex flex-column">
                        <img class="img-arrow-banner" src="assets/images/icons/slim-left.svg" alt="">
                        <!--<p class="p-scroll">scroll</p>-->
                    </a>
                </div>
            </div>
        </section>

        <section class="sct-card-news" id="sct-card-news" name="sct-card-news">
            <!--SECCION NAVBAR-FIXED NOVEDADES-->
            <div class="container-fluid">
                <div class="row">
                    <?php
                        include 'src/includes/navbar-novedades.php'
                    ?>
                    <div class="col-12 col-lg-9 col-xl-10 sct-parallax info-nav-news">
                        <div class="row mx-0">
                            <?php
                                include 'src/includes/nav-galeria.php'
                            ?>
                            <div class="sct-card-news col-12">
                                <div class="row ">
                                    <div class="col-11 d-flex flex-wrap cnt-divs">
                                        <div class="card-img-gallery wow slideInUp">
                                            <figure class="imghvr-slide-up"><img src="assets/images/mineria.jpg" alt="example-image">
                                                <figcaption class="figcaption-img d-flex justify-content-center align-items-center">
                                                    <div class="cont-icon-vm btn-modal"><i class="icon-search"></i></div>
                                                </figcaption>
                                            </figure>
                                            <div class="d-flex info-card-img-gallery">
                                                <i class="icon-arrow color-icons"></i>
                                                <div>
                                                    <h1 class="title-blue">Lorem ipsum dolor</h1>
                                                    <p class="p-internas">Lorem ipsum dolor Quis assumenda laudantium  assumenda laudantium sunt vitae.</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-img-gallery wow slideInUp">
                                            <figure class="imghvr-slide-up"><img src="assets/images/mineria.jpg" alt="example-image">
                                                <figcaption class="figcaption-img d-flex justify-content-center align-items-center">
                                                    <div class="cont-icon-vm btn-modal"><i class="icon-search"></i></div>
                                                </figcaption>
                                            </figure>
                                            <div class="d-flex info-card-img-gallery">
                                                <i class="icon-arrow color-icons"></i>
                                                <div>
                                                    <h1 class="title-blue">Lorem ipsum dolor</h1>
                                                    <p class="p-internas">Lorem ipsum dolor Quis assumenda laudantium  assumenda laudantium sunt vitae.</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-img-gallery wow slideInUp">
                                            <figure class="imghvr-slide-up"><img src="assets/images/mineria.jpg" alt="example-image">
                                                <figcaption class="figcaption-img d-flex justify-content-center align-items-center">
                                                    <div class="cont-icon-vm btn-modal"><i class="icon-search"></i></div>
                                                </figcaption>
                                            </figure>
                                            <div class="d-flex info-card-img-gallery">
                                                <i class="icon-arrow color-icons"></i>
                                                <div>
                                                    <h1 class="title-blue">Lorem ipsum dolor</h1>
                                                    <p class="p-internas">Lorem ipsum dolor Quis assumenda laudantium  assumenda laudantium sunt vitae.</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-img-gallery wow slideInUp">
                                            <figure class="imghvr-slide-up"><img src="assets/images/mineria.jpg" alt="example-image">
                                                <figcaption class="figcaption-img d-flex justify-content-center align-items-center">
                                                    <div class="cont-icon-vm btn-modal"><i class="icon-search"></i></div>
                                                </figcaption>
                                            </figure>
                                            <div class="d-flex info-card-img-gallery">
                                                <i class="icon-arrow color-icons"></i>
                                                <div>
                                                    <h1 class="title-blue">Lorem ipsum dolor</h1>
                                                    <p class="p-internas">Lorem ipsum dolor Quis assumenda laudantium  assumenda laudantium sunt vitae.</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-img-gallery wow slideInUp">
                                            <figure class="imghvr-slide-up"><img src="assets/images/mineria.jpg" alt="example-image">
                                                <figcaption class="figcaption-img d-flex justify-content-center align-items-center">
                                                    <div class="cont-icon-vm btn-modal"><i class="icon-search"></i></div>
                                                </figcaption>
                                            </figure>
                                            <div class="d-flex info-card-img-gallery">
                                                <i class="icon-arrow color-icons"></i>
                                                <div>
                                                    <h1 class="title-blue">Lorem ipsum dolor</h1>
                                                    <p class="p-internas">Lorem ipsum dolor Quis assumenda laudantium  assumenda laudantium sunt vitae.</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-img-gallery wow slideInUp">
                                            <figure class="imghvr-slide-up"><img src="assets/images/mineria.jpg" alt="example-image">
                                                <figcaption class="figcaption-img d-flex justify-content-center align-items-center">
                                                    <div class="cont-icon-vm btn-modal"><i class="icon-search"></i></div>
                                                </figcaption>
                                            </figure>
                                            <div class="d-flex info-card-img-gallery">
                                                <i class="icon-arrow color-icons"></i>
                                                <div>
                                                    <h1 class="title-blue">Lorem ipsum dolor</h1>
                                                    <p class="p-internas">Lorem ipsum dolor Quis assumenda laudantium  assumenda laudantium sunt vitae.</p>
                                                </div>
                                            </div>
                                        </div>
                                        <!--MODAL-->
                                        <section class="sct-modal">
                                            <div class="modal-content">
                                                <div class="modal">
                                                    <div class="modal-header">
                                                        <span id="modal-close-btn">&Cross;</span>
                                                    </div>
                                                    <div class="modal-info">
                                                        <div class="owl-carousel owl-theme owl-img-popup">
                                                            <div class="item"><img src="assets/images/slider-home1.jpg" alt=""></div>
                                                            <div class="item"><img src="assets/images/slider-home3.jpg" alt=""></div>
                                                            <div class="item"><img src="assets/images/mineria.jpg" alt=""></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                    </div>
                                </div>
                            </div>          
                        </div>
                    </div>
                </div>
            </div>
        </section>
        
        <?php
            include 'src/includes/btn-flotant.php'
        ?>
    </main>
    <?php
        include 'src/includes/footer.php'
    ?>
    <script src="assets/js/modal.js"></script>
</body>

</html>