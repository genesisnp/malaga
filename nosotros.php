<?php
    include 'src/includes/header.php'
?>
    <main id="main-us">
        <section class="sct-banner sct-parallax">
            <div class="container-fluid">
                <div class="content-img">
                    <!--<img class="img-banner" src="assets/images/Nosotros.jpg" alt="img/banner">-->
                </div>
                <div class="content-title-banner d-flex flex-column">
                    <h1 class="h1-banner">Nosotros</h1>
                    <a href="#sct-description-us" data-ancla="sct-description-us" class="arrow-repeat no-border d-flex flex-column">
                        <img class="img-arrow-banner" src="assets/images/icons/slim-left.svg" alt="">
                        <!--<p class="p-scroll">scroll</p>-->
                    </a>
                </div>
            </div>

        </section>
        <section class="sct-description-us sct-parallax" id="sct-description-us">
            <div class="container">
                <div class="row align-items-center justify-content-center">
                    <div class="wow slideInLeft col-12 col-xl-5 pb-4">
                        <div class="row">
                            <div class="col-12 content-title-description ">
                                <i class="icon-constructora color-icons"></i>
                                <h1 class="titles-big ">constructora<br><span class="title-orange-clear">Malaga</span>
                                </h1>
                            </div>
                            <div class="col-12 col-xl-11 content-p-description ">
                                <p class="p-internas ">El 2 de noviembre del año 1981 se constituyó lo que hoy es
                                    Constructora Malaga. Desde ese momento, esta empresa originada en Lima - Perú,
                                    inició sus actividades en el rubro de la construcción siendo 100% de capital
                                    peruano.</p>
                                <p class="p-internas ">La gran trayectoria y experiencia adquirida a lo largo de casi
                                    cuatro décadas ha permitido que Constructora Malaga abarque distintas especialidades
                                    de la construcción, como lo son: la infraestructura vial, edificaciones públicas y
                                    privadas, saneamiento, aeropuertos, minería e infraestructura de riego.</p>
                                <p class="p-internas ">La presencia de Constructora Malaga a lo largo del Perú, es
                                    reflejo del compromiso y dedicación de la calidad de profesionales que la componen.
                                    Su experiencia y trabajo no solo ha logrado posicionarla como una de las compañías
                                    más importantes del país, si no que, además, ha logrado internacionalizarla.</p>
                            </div>
                            <div class="col-12 col-xl-11 content-img-certification">
                                <h1 class="title-orange-clear animated flipInX slower"><i class="icon-diamonds"></i>CERTIFICACIONES</h1>
                                <div class="d-flex align-items-center justify-content-end">
                                    <img src="assets/images/logos/isoC-9001.jpg" alt="iso-9001">
                                    <img src="assets/images/logos/isoC-45001.jpg" alt="iso-45001">
                                    <img src="assets/images/logos/isoC-14001.jpg" alt="iso-14001">
                                    <img src="assets/images/logos/isoC-37001.jpg" alt="iso-37001">
                                    <img class="isoIqnet" src="assets/images/logos/iqnetC.jpg" alt="iqnet">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="content-diamond-us col-12 col-lg-7 d-flex align-items-center justify-content-center">
                        <div class="wrapper-diamond-us d-flex justify-content-center align-items-center">
                            <div class="content-diamond-us wow zoomIn">
                                <div class="diamond-us"></div>
                            </div>
                            <div class="content-diamond-us wow zoomIn" data-wow-delay="1s">
                                <div class="diamond-us"></div>
                            </div>
                            <div class="content-diamond-us wow zoomIn" data-wow-delay="3s">
                                <div class="diamond-us"></div>
                            </div>
                            <div class="content-diamond-us wow zoomIn" data-wow-delay="2s">
                                <div
                                    class="diamond-us d-flex justify-content-center align-items-center flex-column text-center">
                                    <h1 class="year-diamond">38</h1>
                                    <p class="p-year-diamond">Años de<br>Experiencia</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--MISION, VISION, MAPA-->
        <section class="sct-mv-map sct-parallax">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12 col-xl-6 content-info-mv wow slideInLeft">
                        <div class="row justify-content-center">
                            <div class="col-12 col-lg-8">
                                <div class="row">
                                    <div class="col-12">
                                        <i class="icon-mision"></i>
                                        <h1 class="title-mv">MISIÓN</h1>
                                    </div>
                                    <div class="col-12 col-lg-10 d-flex content-pdescription-mv">
                                        <i class="icon-arrow"></i>
                                        <p class="description-mv p-internas">Contribuir al desarrollo integral del país
                                            y las comunidades donde se ubican nuestro proyectos, ejecutando obras que
                                            generen valor y fomenten una mejor calidad de vida. Asimismo, generar
                                            puestos de trabajo que impulsen el desarrollo profesional de nuestros
                                            colaboradores, mantener un continuo respeto hacia el medio ambiente y
                                            asegurar la continuidad de la empresa.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-lg-8">
                                <div class="row">
                                    <div class="col-12">
                                        <i class="icon-vision"></i>
                                        <h1 class="title-mv">VISIÓN</h1>
                                    </div>
                                    <div class="col-12 col-lg-10 d-flex content-pdescription-mv">
                                        <i class="icon-arrow"></i>
                                        <p class="description-mv p-internas">Ser reconocidos como una empresa líder en
                                            el sector construcción ampliando nuestro portafolio de proyectos y
                                            aumentando nuestra participación en el sector privado a nivel nacional e
                                            internacional, brindando nuestros servicios de manera oportuna, responsable,
                                            confiable y transparente.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-xl-6 content-info-map wow slideInRight">
                        <div class="row">
                            <div class="col-12 col-lg-11 content-title-map d-flex align-items-center">
                                <i class="icon-ubicacion"></i>
                                <h1 class="titles-big">PRESENCIA<br><span class="title-orange-clear">EN EL PERÚ</span>
                                </h1>
                            </div>
                            <div class="col-12 departaments-mobile d-none px-0">
                                <div class="row justify-content-center">
                                    <div class="div-dep">
                                        <span class="departament">lima</span>
                                        <p class="p-internas">dirección</p>
                                        <p class="p-internas">999999</p>
                                    </div>
                                    <div class="div-dep">
                                        <span class="departament">piura</span>
                                        <p class="p-internas">dirección</p>
                                        <p class="p-internas">999999</p>
                                    </div>
                                    <div class="div-dep">
                                        <span class="departament">lambayeque</span>
                                        <p class="p-internas">dirección</p>
                                        <p class="p-internas">999999</p>
                                    </div>
                                    <div class="div-dep">
                                        <span class="departament">cajamarca</span>
                                        <p class="p-internas">dirección</p>
                                        <p class="p-internas">999999</p>
                                    </div>
                                    <div class="div-dep">
                                        <span class="departament">la libertad</span>
                                        <p class="p-internas">dirección</p>
                                        <p class="p-internas">999999</p>
                                    </div>
                                    <div class="div-dep">
                                        <span class="departament">san martín</span>
                                        <p class="p-internas">dirección</p>
                                        <p class="p-internas">999999</p>
                                    </div>
                                    <div class="div-dep">
                                        <span class="departament">huánuco</span>
                                        <p class="p-internas">dirección</p>
                                        <p class="p-internas">999999</p>
                                    </div>
                                    <div class="div-dep">
                                        <span class="departament">pasco</span>
                                        <p class="p-internas">dirección</p>
                                        <p class="p-internas">999999</p>
                                    </div>
                                    <div class="div-dep">
                                        <span class="departament">ucayali</span>
                                        <p class="p-internas">dirección</p>
                                        <p class="p-internas">999999</p>
                                    </div>
                                    
                                    <div class="div-dep">
                                        <span class="departament">junin</span>
                                        <p class="p-internas">dirección</p>
                                        <p class="p-internas">999999</p>
                                    </div>
                                    <div class="div-dep">
                                        <span class="departament">huancavelica</span>
                                        <p class="p-internas">dirección</p>
                                        <p class="p-internas">999999</p>
                                    </div>
                                    <div class="div-dep">
                                        <span class="departament">ica</span>
                                        <p class="p-internas">dirección</p>
                                        <p class="p-internas">999999</p>
                                    </div>
                                    <div class="div-dep">
                                        <span class="departament">ayacucho</span>
                                        <p class="p-internas">dirección</p>
                                        <p class="p-internas">999999</p>
                                    </div>
                                    <div class="div-dep">
                                        <span class="departament">apurimac</span>
                                        <p class="p-internas">dirección</p>
                                        <p class="p-internas">999999</p>
                                    </div>
                                    <div class="div-dep">
                                        <span class="departament">cusco</span>
                                        <p class="p-internas">dirección</p>
                                        <p class="p-internas">999999</p>
                                    </div>
                                    <div class="div-dep">
                                        <span class="departament">madre de dios</span>
                                        <p class="p-internas">dirección</p>
                                        <p class="p-internas">999999</p>
                                    </div>
                                    <div class="div-dep">
                                        <span class="departament">arequipa</span>
                                        <p class="p-internas">dirección</p>
                                        <p class="p-internas">999999</p>
                                    </div>
                                    <div class="div-dep">
                                        <span class="departament">puno</span>
                                        <p class="p-internas">dirección</p>
                                        <p class="p-internas">999999</p>
                                    </div>
                                    <div class="div-dep">
                                        <span class="departament">moquegua</span>
                                        <p class="p-internas">dirección</p>
                                        <p class="p-internas">999999</p>
                                    </div>
                                    
                                </div>
                            </div>
                            <div class="content-map-desktop ">
                                <div class="d-none d-lg-block">
                                    <img class="img-map" src="assets/images/Mapa_peru.png" alt="mapa-peru">
                                </div>
                                <div id="tabsContainer">
                                    <div class="tabs-nav">
                                        <ul class="list-departament">
                                            <li class="item-departament d-lg-none">
                                                <a class="d-flex align-items-center link-departament">
                                                    <img class="rombo-img" src="assets/images/icons/rombito.svg" alt=""> Lima</a>
                                                    <p class="info-ofc">Oficina Central</p>
                                                    <p class="span-direction">Av. Manuel Olguín 211 - Int.1702<br>Santiago de Surco</p>
                                            </li>
                                            <li class="item-departament">
                                                <a class="d-flex flex-lg-column align-items-center link-departament">
                                                    <i class="icon-diamonds d-none d-lg-block"></i>
                                                    <img class="rombo-img d-lg-none" src="assets/images/icons/rombito.svg" alt=""> Piura</a>
                                            </li>
                                            <li class="item-departament">
                                                <a class="d-flex flex-lg-column align-items-center link-departament">
                                                    <i class="icon-diamonds d-none d-lg-block"></i>
                                                    <img class="rombo-img d-lg-none" src="assets/images/icons/rombito.svg" alt=""> Lambayeque</a>
                                            </li>
                                            <li class="item-departament">
                                                <a class="d-flex flex-lg-column align-items-center link-departament">
                                                    <i class="icon-diamonds d-none d-lg-block"></i>
                                                    <img class="rombo-img d-lg-none" src="assets/images/icons/rombito.svg" alt=""> Cajamarca</a>
                                            </li>
                                            <li class="item-departament">
                                                <a class="d-flex flex-lg-column align-items-center link-departament">
                                                    <i class="icon-diamonds d-none d-lg-block"></i>
                                                    <img class="rombo-img d-lg-none" src="assets/images/icons/rombito.svg" alt=""> La Libertad</a>
                                            </li>
                                            <li class="item-departament">
                                                <a class="d-flex flex-lg-column align-items-center link-departament">
                                                    <i class="icon-diamonds d-none d-lg-block"></i>
                                                    <img class="rombo-img d-lg-none" src="assets/images/icons/rombito.svg" alt=""> San Martín</a>
                                            </li>
                                            <li class="item-departament">
                                                <a class="d-flex flex-lg-column align-items-center link-departament">
                                                    <i class="icon-diamonds d-none d-lg-block"></i>
                                                    <img class="rombo-img d-lg-none" src="assets/images/icons/rombito.svg" alt=""> Huánuco</a>
                                            </li>
                                            <li class="item-departament">
                                                <a class="d-flex flex-lg-column align-items-center link-departament">
                                                    <i class="icon-diamonds d-none d-lg-block"></i>
                                                    <img class="rombo-img d-lg-none" src="assets/images/icons/rombito.svg" alt=""> Ucayali</a>
                                            </li>
                                            <li class="item-departament">
                                                <a class="d-flex flex-lg-column align-items-center link-departament">
                                                    <i class="icon-diamonds d-none d-lg-block"></i>
                                                    <img class="rombo-img d-lg-none" src="assets/images/icons/rombito.svg" alt=""> Pasco</a>
                                            </li>
                                            <li class="active item-departament d-none d-lg-block">
                                                <div class="line-map">
                                                    <svg class="" viewBox="0 0 204.9 368.5"><path class="pathmin" d="M0.8 295 250.8 1 255.8 1" style="fill:none;stroke: #f4a636;stroke-miterlimit:10;stroke-width:1px;"></path></svg>
                                                </div>
                                                <a class="d-flex flex-lg-column align-items-center link-departament" href="#tab1">
                                                    <i class="icon-diamonds"></i>
                                                    <img class="rombo-img" src="assets/images/icons/rombo.svg" alt="">Lima</a>
                                            </li>
                                            <li class="item-departament">
                                                <a class="d-flex flex-lg-column align-items-center link-departament">
                                                <i class="icon-diamonds d-none d-lg-block"></i>
                                                <img class="rombo-img d-lg-none" src="assets/images/icons/rombito.svg" alt=""> Junín</a>
                                            </li>
                                            <li class="item-departament">
                                                <a class="d-flex flex-lg-column align-items-center link-departament">
                                                    <i class="icon-diamonds d-none d-lg-block"></i>
                                                    <img class="rombo-img d-lg-none" src="assets/images/icons/rombito.svg" alt=""> Huancavelica</a>
                                            </li>
                                            <li class="item-departament">
                                                <a class="d-flex flex-lg-column align-items-center link-departament"
                                                ><i class="icon-diamonds d-none d-lg-block"></i>
                                                <img class="rombo-img d-lg-none" src="assets/images/icons/rombito.svg" alt=""> Ica</a>
                                            </li>
                                            <li class="item-departament">
                                                <a class="d-flex flex-lg-column align-items-center link-departament"
                                                ><i class="icon-diamonds d-none d-lg-block"></i>
                                                <img class="rombo-img d-lg-none" src="assets/images/icons/rombito.svg" alt=""> Ayacucho</a>
                                            </li>
                                            <li class="item-departament">
                                                <a class="d-flex flex-lg-column align-items-center link-departament"
                                                ><i class="icon-diamonds d-none d-lg-block"></i>
                                                <img class="rombo-img d-lg-none" src="assets/images/icons/rombito.svg" alt=""> Apurimac</a>
                                            </li>
                                            <li class="item-departament">
                                                <a class="d-flex flex-lg-column align-items-center link-departament"
                                                ><i class="icon-diamonds d-none d-lg-block"></i>
                                                <img class="rombo-img d-lg-none" src="assets/images/icons/rombito.svg" alt=""> Cusco</a>
                                            </li>
                                            <li class="item-departament">
                                                <a class="d-flex flex-lg-column align-items-center link-departament"
                                                ><i class="icon-diamonds d-none d-lg-block"></i>
                                                <img class="rombo-img d-lg-none" src="assets/images/icons/rombito.svg" alt=""> Madre de Dios</a>
                                            </li>
                                            <li class="item-departament">
                                                <a class="d-flex flex-lg-column align-items-center link-departament">
                                                <i class="icon-diamonds d-none d-lg-block"></i>
                                                <img class="rombo-img d-lg-none" src="assets/images/icons/rombito.svg" alt=""> Puno</a>
                                            </li>
                                            <li class="item-departament">
                                                <a class="d-flex flex-lg-column align-items-center link-departament">
                                                <i class="icon-diamonds d-none d-lg-block"></i>
                                                <img class="rombo-img d-lg-none" src="assets/images/icons/rombito.svg" alt=""> Arequipa</a>
                                            </li>
                                            <li class="item-departament">
                                                <a class="d-flex flex-lg-column align-items-center link-departament">
                                                <i class="icon-diamonds d-none d-lg-block"></i>
                                                <img class="rombo-img d-lg-none" src="assets/images/icons/rombito.svg" alt=""> Moquegua</a>
                                            </li>
                                        </ul>
                                    </div>
                                    <section class="tabs-content d-none d-lg-block">
                                        <div id="tab1" class="info-tab">
                                            <figure class="content-img-departament">
                                                <img class="img-departament" src="assets/images/departament/lima.jpg" alt="">
                                            </figure>
                                            <h2 class="title-orange-clear">Lima</h2>
                                            <span class="title-orange-clear text-center"><strong>Oficina Central</strong></span>
                                            <p class="span-direction">Av. Manuel Olguín 211 - Int.1702 Santiago de Surco</p>
                                        </div>
                                    </section>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--VALORES-->
        <section class="sct-values d-flex justify-content-center align-items-center">
            <div class="container">
                <div class="row">
                    <div class="col-12 text-center wow zoomIn">
                        <i class="icon-valores"></i>
                        <h1 class="titles-big">valores</h1>
                    </div>
                    <div class="div-values col-10 col-md-6 col-lg-3 wow zoomIn" data-wow-duration="2s">
                        <div class="row align-items-center flex-column">
                            <div class="content-img-value d-flex justify-content-center align-items-end">
                                <img class="img-value" src="assets/images/icons/integridad.svg"
                                    alt="img/valor-integridad">
                            </div>
                            <span class="title-value">Integridad</span>
                            <p class="col-12 col-lg-10 text-center p-internas">Ser muy consecuente con lo que se pueda decir o con
                                lo que se pueda considera
                                que es correcto, en
                                cualquier circunstancia.</p>
                        </div>
                    </div>
                    <div class="div-values col-10 col-md-6 col-lg-3 wow zoomIn" data-wow-delay="1s" data-wow-duration="2s">
                        <div class="row align-items-center flex-column">
                            <div class="content-img-value d-flex justify-content-center align-items-end">
                                <img class="img-value" src="assets/images/icons/compromiso.svg"
                                    alt="mg/valor-compromiso">
                            </div>
                            <span class="title-value">Compromiso</span>
                            <p class="col-12 col-lg-10 text-center p-internas">Cumplir con las obligaciones contraídas, buscando
                                el mejor
                                resultado, respetando las normas legales aplicables vigentes y desempeñando con
                                dedicación</p>
                        </div>
                    </div>
                    <div class="div-values col-10 col-md-6 col-lg-3 wow zoomIn" data-wow-delay="2s" data-wow-duration="2s">
                        <div class="row align-items-center flex-column">
                            <div class="content-img-value d-flex justify-content-center align-items-end">
                                <img class="img-value" src="assets/images/icons/perseverancia.svg"
                                    alt="img/valor-perseverancia">
                            </div>
                            <span class="title-value">Perseverancia</span>
                            <p class="col-12 col-lg-10 text-center p-internas">Actitud que marca el rumbo de la empresa con
                                acciones
                                concretas, logrando superar obstáculos y
                                enfocándose a cumplir los objetivos y metas propuestas.</p>
                        </div>
                    </div>
                    <div class="div-values col-10 col-md-6 col-lg-3 wow zoomIn" data-wow-delay="3s" data-wow-duration="2s">
                        <div class="row align-items-center flex-column">
                            <div class="content-img-value d-flex justify-content-center align-items-end">
                                <img class="img-value" src="assets/images/icons/liderazgo.svg"
                                    alt="img/valor-liderazgo">
                            </div>
                            <span class="title-value">Liderazgo</span>
                            <p class="col-12 col-lg-10 text-center p-internas">Capacidad para motivar y dirigir, creando un
                                ambiente de empatía, colaboración y respeto, para lograr que estas contribuyan de forma
                                efectiva y
                                adecuada a la consecución de los objetivos.</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--POLÍTICAS-->
        <section class="sct-policies sct-parallax">
            <div class="container">
                <div class="row justify-content-between">
                    <div class="col-12 content-title-policies d-flex wow zoomIn">
                        <i class="icon-politicas"></i>
                        <h1 class="titles-big">POLÍTICAS<br><span class="title-orange-clear">Y CÓDIGOS</span></h1>
                    </div>
                    <div class="col-12 col-lg-5 description-policies wow zoomIn">
                        <div class="row">
                            <div class="col-12">
                                <h1 class="titles-big">POLÍTICA INTEGRADA DE GESTIÓN</h1>
                            </div>
                            <div class="col-12 d-flex">
                                <i class="icon-arrow"></i>
                                <p class="p-internas">Hemos desarrollado una política que nos permite integrar la gestión de los ámbitos más relevantes para el optimo 
                                    desempeño de la empresa. Dicha política tiene como objetivo asegurar nuestro compromiso con el medio ambiente, las personas y la ley.</p>
                            </div>
                            <div class="col-12 content-dscg-pdf">
                                <i class="icon-diamonds"></i>
                                <a class="titles-big btn-modal">VER MÁS</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-lg-5 description-policies wow zoomIn">
                        <div class="row">
                            <div class="col-12">
                                <h1 class="titles-big">CÓDIGO DE ÉTICA</h1>
                            </div>
                            <div class="col-12 d-flex">
                                <i class="icon-arrow"></i>
                                <p class="p-internas">La esencia de Constructora Malaga se encuentra en sus colaboradores y los valores que como empresa se han 
                                    desarrollado y respetado durante toda su trayectoria. 
                                    Así, construimos una cultura de integridad y ética que se ve reflejada en nuestro Código de Ética.</p>
                            </div>
                            <div class="col-12 content-dscg-pdf">
                                <i class="icon-diamonds"></i>
                                <a class="titles-big btn-modal-2">VER MÁS</a>
                            </div>
                        </div>
                    </div>
                    <!--MODAL-->
                    <section class="sct-modal">
                        <div class="modal-content">
                            <div class="modal">
                                <div class="modal-header">
                                    <span class="modal-close-btn" id="modal-close-btn">&Cross;</span>
                                </div>
                                <div class="modal-info">
                                    <div class="owl-carousel owl-theme owl-img-popup">
                                        <div class="item">
                                            <img src="assets/images/policies/pol-integrada-2.jpg" alt="">
                                        </div>
                                        <!--<div class="item"><img src="assets/images/slider-home3.jpg" alt=""></div>
                                        <div class="item"><img src="assets/images/mineria.jpg" alt=""></div>-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <!--segundo modal  para que vea el cliente en web-->
                    <section class="sct-modal">
                        <div class="modal-content-2">
                            <div class="modal-2">
                                <div class="modal-header">
                                    <span class="modal-close-btn" id="modal-close-btn-2">&Cross;</span>
                                </div>
                                <div class="modal-info">
                                    <div class="owl-carousel owl-theme owl-img-popup">
                                        <div class="item"><img src="assets/images/policies/cod-et1.jpg" alt=""></div>
                                        <div class="item"><img src="assets/images/policies/cod-et2.jpg" alt=""></div>
                                        <div class="item"><img src="assets/images/policies/cod-et3.jpg" alt=""></div>
                                        <div class="item"><img src="assets/images/policies/cod-et4.jpg" alt=""></div>
                                        <div class="item"><img src="assets/images/policies/cod-et5.jpg" alt=""></div>
                                        <div class="item"><img src="assets/images/policies/cod-et6.jpg" alt=""></div>
                                        <div class="item"><img src="assets/images/policies/cod-et7.jpg" alt=""></div>
                                        <div class="item"><img src="assets/images/policies/cod-et8.jpg" alt=""></div>
                                        <div class="item"><img src="assets/images/policies/cod-et9.jpg" alt=""></div>
                                        <div class="item"><img src="assets/images/policies/cod-et10.jpg" alt=""></div>
                                        <div class="item"><img src="assets/images/policies/cod-et11.jpg" alt=""></div>
                                        <div class="item"><img src="assets/images/policies/cod-et12.jpg" alt=""></div>
                                        <div class="item"><img src="assets/images/policies/cod-et13.jpg" alt=""></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </section>
        <?php
            include 'src/includes/btn-flotant.php'
        ?>
    </main>

    <?php
        include 'src/includes/footer.php'
    ?>
    <script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
    <script src="assets/js/modal.js"></script>
    <script src="assets/js/modal2-us.js"></script>
    
</body>

</html>